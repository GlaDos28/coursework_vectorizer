package scala.frontend


import scala.backend.codeText.CodeText
import scala.backend.ir.IR
import scala.frontend.lexer.Lexer
import scala.frontend.parser.Parser


object Translator {

    def translate(text: CodeText): IR =
        new Parser(new Lexer(text.text).getProcessedTokens()).parse()

}

package scala.frontend.lexer


class Token(val lexemeId: Int, val value: Any) {

    override def toString: String =
        "(" + this.lexemeId + ", \"" + this.value + "\")"

}

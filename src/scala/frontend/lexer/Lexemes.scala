package scala.frontend.lexer


object Lexemes {

    val Lexemes = 34

    val LexemeTab        = 0
    val LexemeInt        = 1
    val LexemeBool       = 2
    val LexemeArray      = 3
    val LexemeIf         = 4
    val LexemeElse       = 5
    val LexemeWhile      = 6
    val LexemeFor        = 7
    val LexemeTo         = 8
    val LexemeVar        = 9
    val LexemeOpenParen  = 10
    val LexemeCloseParen = 11
    val LexemePlus       = 12
    val LexemeMinus      = 13
    val LexemeMul        = 14
    val LexemeDiv        = 15
    val LexemeMod        = 16
    val LexemeAssign     = 17
    val LexemeColon      = 18
    val LexemeAnd        = 19
    val LexemeOr         = 20
    val LexemeXor        = 21
    val LexemeNot        = 22
    val LexemeLt         = 23
    val LexemeGt         = 24
    val LexemeEq         = 25
    val LexemeNeq        = 26
    val LexemeElt        = 27
    val LexemeEgt        = 28
    val LexemeIdent      = 29
    val LexemeType       = 30
    val LexemeOpenBrace  = 31
    val LexemeCloseBrace = 32
    val LexemeEof        = 33

}

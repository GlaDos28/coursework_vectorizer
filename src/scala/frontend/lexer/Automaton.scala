package scala.frontend.lexer


class Automaton private(
    val t:          Array[Array[Int]],
    val f:          Array[Int],
    val startState: Int,
    val stateNum:   Int,
    val symbolNum:  Int
)


object Automaton {

    def apply(t: Array[Array[Int]], f: Array[Int], startState: Int): Automaton = {
        if (t.isEmpty) {
            throw new RuntimeException("automaton with no states")
        }

        new Automaton(t, f, startState, t.length, t(0).length)
    }

}

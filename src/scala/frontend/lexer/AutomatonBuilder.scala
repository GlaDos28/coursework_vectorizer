package scala.frontend.lexer


import scala.collection.mutable.{ArrayBuffer, Map => MMap}


class AutomatonBuilder(var t: ArrayBuffer[MMap[Char, Int]], var f: ArrayBuffer[Int], var lastState: Int) {

    def this() = this(ArrayBuffer(MMap()), ArrayBuffer(-1), 0)


    def connect(s1: Int, chars: String, s2: Int): Unit = {
        chars.foreach(ch => this.t(s1)(ch) = s2)
        this.lastState = s2
    }

    def connectStart(chars: String, s: Int): Unit = {
        this.connect(0, chars, s)
    }

    def connectLast(chars: String, s: Int): Unit = {
        this.connect(this.lastState, chars, s)
    }

    def newState(lexemeId: Int = -1): Int = {
        this.t += MMap()
        this.f += lexemeId

        this.f.length - 1
    }

    def nconnect(s: Int, chars: String, lexemeId: Int = -1): Int = {
        val s2 = this.newState(lexemeId)
        this.connect(s, chars, s2)

        s
    }

    def nconnectStart(chars: String, lexemeId: Int = -1): Int = {
        val s = this.newState(lexemeId)
        this.connectStart(chars, s)

        s
    }

    def nconnectLast(chars: String, lexemeId: Int = -1): Int = {
        val s = this.newState(lexemeId)
        this.connectLast(chars, s)

        s
    }

    def nconnectStartWithBranch(chars: String, branchChars: String, branchS: Int): Int = {
        val cur = this.nconnectStart(chars)
        this.connectLast(branchChars, branchS)

        this.f(cur) = this.f(branchS)
        this.lastState = cur

        cur
    }

    def nconnectLastWithBranch(chars: String, branchChars: String, branchS: Int): Int = {
        val cur = this.nconnectLast(chars)
        this.connectLast(branchChars, branchS)

        this.f(cur) = this.f(branchS)
        this.lastState = cur

        cur
    }

    def build(): Automaton = {
        val extractedT: Array[Array[Int]] = Array.fill(this.f.length)(Array.fill(AutomatonBuilder.SymbolNum)(-1))

        for (i <- this.f.indices) {
            for (pair <- this.t(i)) {
                extractedT(i)(pair._1) = pair._2
            }
        }

        Automaton(extractedT, this.f.toArray, 0)
    }
}


object AutomatonBuilder {

    val SymbolNum = 256


    private val digits              = "0123456789"
    private val letters             = "abcdefghijklmnopqrstuvwxyz"
    private val capital_letters     = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    private val ident_inner_symbols = digits + letters + capital_letters + '_'


    def getDefaultAutomaton: Automaton = {
        val builder = new AutomatonBuilder()

        builder.nconnectStart("\n", Lexemes.LexemeTab)
        builder.connectLast(" ", builder.lastState)

        val ident = builder.nconnectStart(letters, Lexemes.LexemeIdent)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnectStart(digits, Lexemes.LexemeInt)
        builder.connectLast(digits, builder.lastState)

        builder.nconnectStart("T")
        builder.nconnectLast("r")
        builder.nconnectLast("u")
        val tmp = builder.nconnectLast("e", Lexemes.LexemeBool)

        builder.nconnectStart("F")
        builder.nconnectLast("a")
        builder.nconnectLast("l")
        builder.nconnectLast("s")
        builder.connectLast("e", tmp)

        builder.connect(tmp, ident_inner_symbols, ident)

        builder.nconnectStartWithBranch("i", ident_inner_symbols, ident)
        builder.nconnectLast("f", Lexemes.LexemeIf)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnectStartWithBranch("e", ident_inner_symbols, ident)
        builder.nconnectLastWithBranch("l", ident_inner_symbols, ident)
        builder.nconnectLastWithBranch("s", ident_inner_symbols, ident)
        builder.nconnectLast("e", Lexemes.LexemeElse)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnectStartWithBranch("w", ident_inner_symbols, ident)
        builder.nconnectLastWithBranch("h", ident_inner_symbols, ident)
        builder.nconnectLastWithBranch("i", ident_inner_symbols, ident)
        builder.nconnectLastWithBranch("l", ident_inner_symbols, ident)
        builder.nconnectLast("e", Lexemes.LexemeWhile)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnectStartWithBranch("f", ident_inner_symbols, ident)
        builder.nconnectLastWithBranch("o", ident_inner_symbols, ident)
        builder.nconnectLast("r", Lexemes.LexemeFor)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnectStartWithBranch("t", ident_inner_symbols, ident)
        builder.nconnectLast("o", Lexemes.LexemeTo)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnectStartWithBranch("v", ident_inner_symbols, ident)
        builder.nconnectLastWithBranch("a", ident_inner_symbols, ident)
        builder.nconnectLast("r", Lexemes.LexemeVar)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnect(tmp, "t", Lexemes.LexemeNot)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnectStartWithBranch("a", ident_inner_symbols, ident)
        builder.nconnectLastWithBranch("n", ident_inner_symbols, ident)
        builder.nconnectLast("d", Lexemes.LexemeAnd)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnectStartWithBranch("o", ident_inner_symbols, ident)
        builder.nconnectLast("r", Lexemes.LexemeOr)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnectStartWithBranch("x", ident_inner_symbols, ident)
        builder.nconnectLastWithBranch("o", ident_inner_symbols, ident)
        builder.nconnectLast("r", Lexemes.LexemeXor)
        builder.connectLast(ident_inner_symbols, ident)

        builder.nconnectStart("(", Lexemes.LexemeOpenParen)
        builder.nconnectStart(")", Lexemes.LexemeCloseParen)
        builder.nconnectStart("+", Lexemes.LexemePlus)
        builder.nconnectStart("-", Lexemes.LexemeMinus)
        builder.nconnectStart("*", Lexemes.LexemeMul)
        builder.nconnectStart("/", Lexemes.LexemeDiv)
        builder.nconnectStart("%", Lexemes.LexemeMod)
        builder.nconnectStart(":", Lexemes.LexemeColon)
        builder.nconnectStart("[", Lexemes.LexemeOpenBrace)
        builder.nconnectStart("]", Lexemes.LexemeCloseBrace)

        builder.nconnectStart("=", Lexemes.LexemeAssign)
        builder.nconnectLast("=", Lexemes.LexemeEq)

        builder.nconnectStart("!")
        builder.nconnectLast("=", Lexemes.LexemeNeq)

        builder.nconnectStart("<", Lexemes.LexemeLt)
        builder.nconnectLast("=", Lexemes.LexemeElt)

        builder.nconnectStart(">", Lexemes.LexemeGt)
        builder.nconnectLast("=", Lexemes.LexemeEgt)

        builder.nconnectStart("I")
        builder.nconnectLast("n")
        builder.nconnectLast("t", Lexemes.LexemeType)

        builder.nconnectStart("B")
        builder.nconnectLast("o")
        builder.nconnectLast("o")
        builder.nconnectLast("l", Lexemes.LexemeType)

        builder.nconnectStart("A")
        builder.nconnectLast("r")
        builder.nconnectLast("r")
        builder.nconnectLast("a")
        builder.nconnectLast("y", Lexemes.LexemeType)

        builder.build()
    }

}

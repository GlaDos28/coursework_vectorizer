package scala.frontend.lexer


import Preamble._
import scala.collection.mutable.ListBuffer


class Lexer private(val text: String, val automaton: Automaton, var pos: Int) {

    def this(text: String) = this(
        '\n' + text + Lexer.EofMarker,
        AutomatonBuilder.getDefaultAutomaton,
        0
    )


    private def nextState(state: Int): Int =
        this.automaton.t(state)(this.text(this.pos))


    private def stateLid(state: Int): Int =
        if (state == -1)
            -1 else
            this.automaton.f(state)


    private def skipWs(): Unit =
        while (this.text(this.pos) == ' ' || this.text(this.pos) == '\t') {
            this.pos += 1
        }


    def getToken(): Token = {
        if (this.text(this.pos) == Lexer.EofMarker) {
            return Lexer.EofToken
        }

        this.skipWs()

        var state    = this.automaton.startState
        var lexemeId = -1
        var value    = ""

        while (this.text(this.pos) != Lexer.EofMarker && (lexemeId == -1 || this.nextState(state) != -1)) {
            value += this.text(this.pos)

            state = this.nextState(state)
            this.pos += 1

            lexemeId = this.stateLid(state)
        }

        if (lexemeId == -1) {  /* Lexer error */
            throw new RuntimeException("lexer error: invalid char sequence \"%s\"" % value)
        }

        /* Processing */

        val finalValue = lexemeId match {
            case Lexemes.LexemeTab  => value.substring(1)
            case Lexemes.LexemeInt  => value.toInt
            case Lexemes.LexemeBool => value.toBoolean
            case _                  => value
        }

        new Token(lexemeId, finalValue)
    }


    def getTokens(): List[Token] = {
        var tokens   = ListBuffer.empty[Token]
        var newToken = this.getToken()

        while (newToken != Lexer.EofToken) {
            tokens += newToken
            newToken = this.getToken()
        }

        (tokens += newToken).toList  /* tokens with EOF token */
    }


    def getProcessedTokens(): List[Token] = {
        val tokens          = this.getTokens()
        var processedTokens = ListBuffer.empty[Token]
        var ind             = 0

        while (tokens(ind).lexemeId == Lexemes.LexemeTab) {
            ind += 1
        }

        ind -= 1

        while (ind < tokens.length - 1) {
            if (tokens(ind).lexemeId != Lexemes.LexemeTab
                || tokens(ind + 1).lexemeId != Lexemes.LexemeTab
                && tokens(ind + 1).lexemeId != Lexemes.LexemeEof)
            {
                processedTokens += tokens(ind)
            }

            ind += 1
        }

        (processedTokens += tokens(ind)).toList  /* processed tokens with EOF token */
    }

}


object Lexer {

    val EofToken  = new Token(Lexemes.LexemeEof, "")
    val EofMarker = '$'

}

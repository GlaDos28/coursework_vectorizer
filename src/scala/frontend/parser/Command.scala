package scala.frontend.parser


class Command(val cmdType: Int, val cmdValue: Any)


object Command {
    /* Command types */

    val CmdInt         = 0   /* Push integer constant in stack */
    val CmdBool        = 1   /* Push boolean constant in stack */
    val CmdArray       = 2   /* Push int array constant in stack */
    val CmdIdent       = 3   /* Push identifier value in stack */
    val CmdIdentName   = 4   /* Push identifier name value in stack */
    val CmdTypeName    = 5   /* Push type name value in stack */
    val CmdAssign      = 6   /* Assign value to identifier */
    val CmdGoto        = 7   /* Go to command with given number */
    val CmdIfGoto      = 8   /* Check boolean on stack: if true, go to command with given number */
    val CmdSum         = 9   /* + */
    val CmdDif         = 10  /* - */
    val CmdMul         = 11  /* * */
    val CmdDiv         = 12  /* / */
    val CmdMod         = 13  /* % */
    val CmdUnaryMinus  = 14  /* - */
    val CmdEq          = 15  /* == */
    val CmdLt          = 16  /* < */
    val CmdGt          = 17  /* > */
    val CmdNeq         = 18  /* != */
    val CmdElt         = 19  /* <= */
    val CmdEgt         = 20  /* >= */
    val CmdAnd         = 21  /* AND boolean operation */
    val CmdOr          = 22  /* OR  boolean operation */
    val CmdXor         = 23  /* XOR boolean operation */
    val CmdNot         = 24  /* NOT boolean operation */
    val CmdElseGoto    = 25  /* Check boolean on stack: if false, go to command with given number */
    val CmdVarStart    = 26  /* Marker determining var block starting */
    val CmdVarEnd      = 27  /* Marker determining var block ending */
    val CmdNewVar      = 28  /* Declare new variable in var block (value - name; stack - init value (lower), type (top)) */
    val CmdAccessStart = 29  /* Indicates var field access start; value contains access part number */
    val CmdCondStart   = 30  /* Indicates condition start */
    val CmdLoopStart   = 31  /* Indicates loop start */

    private val CmdStrList = List(
        "INT          ",
        "BOOL         ",
        "ARRAY        ",
        "IDENT        ",
        "IDENT_NAME   ",
        "TYPE         ",
        "ASSIGN       ",
        "GOTO         ",
        "IFGOTO       ",
        "SUM          ",
        "DIF          ",
        "MUL          ",
        "DIV          ",
        "MOD          ",
        "UNARY_MINUS  ",
        "EQ           ",
        "LT           ",
        "GT           ",
        "NEQ          ",
        "ELT          ",
        "EGT          ",
        "AND          ",
        "OR           ",
        "XOR          ",
        "NOT          ",
        "ELSEGOTO     ",
        "VAR_START    ",
        "VAR_END      ",
        "NEW_VAR      ",
        "ACCESS_START ",
        "COND_START   ",
        "LOOP_START   "
    )

    def getCmdName(ind: Int): String = Command.CmdStrList(ind)
}

package scala.frontend.parser


import Preamble._

import scala.frontend.lexer.{Lexemes, Token}
import backend.ir.IR
import backend.ir.nodes._


class Parser private(
    val tabStr:       String,
    val tokens:       List[Token],
    var pos:          Int,
    var curTabNum:    Int,
    var bugArgCnt:    Int,
    var bufAccessCnt: Int,
    val ir:           IR) {

    def this(tokens: List[Token], tabChar: Char = ' ', tabWidth: Int = 4) =
        this(tabChar.toString * tabWidth, tokens, 0, 0, 0 ,0, new IR())


    private def setError(msg: String): Nothing = {  /* Parser error */
        throw new RuntimeException(msg)
    }


    def parse(): IR = {
        this.parseProgram()

        if (!this.lexemeIs(Lexemes.LexemeEof)) {
            this.setError("end of code expected, but redundant tokens found")
        }

        this.ir
    }


    def getToken: Token = this.tokens(this.pos)


    def lexemeIs(lexemeId: Int): Boolean = this.getToken.lexemeId == lexemeId


    def parseToken(expectedLexeme: Int): Any = {
        if (!this.lexemeIs(expectedLexeme)) {
            throw new RuntimeException("expected lexeme with ID %d, found token %s" % (expectedLexeme, this.getToken.toString()))
        }

        val value = this.getToken.value
        this.pos += 1

        value
    }


    def isTabOf(tabNum: Int): Boolean =
        this.lexemeIs(Lexemes.LexemeTab) && this.getToken.value == this.tabStr * tabNum


    def parseProgram(): Unit = {
        if (this.tokens.length > 1 && this.tokens(this.pos + 1).lexemeId == Lexemes.LexemeVar) {
            this.parseToken(Lexemes.LexemeTab)
            this.parseVarBlock()
        }

        val block = this.parseStatements()

        this.ir.setRoot(block)
    }


    def parseVarBlock(): Unit = {
        this.parseToken(Lexemes.LexemeVar)
        this.parseToken(Lexemes.LexemeColon)
        this.parseDefinitions()
    }


    def parseDefinitions(): Unit = {
        if (this.isTabOf(1)) {
            this.parseDefinition()
            this.parseOptDefinitions()
        } else {
            this.setError("empty var block")
        }
    }


    def parseOptDefinitions(): Unit = {
        if (this.isTabOf(1)) {
            this.parseDefinition()
            this.parseOptDefinitions()
        }
    }


    def parseDefinition(): Unit = {
        this.parseToken(Lexemes.LexemeTab)
        val identName = this.parseToken(Lexemes.LexemeIdent).asInstanceOf[String]
        this.parseToken(Lexemes.LexemeColon)

        val varType = this.parseToken(Lexemes.LexemeType).asInstanceOf[String]

        val arraySize = varType match {
            case "Array" =>
                this.parseToken(Lexemes.LexemeOpenParen)
                val size = this.parseToken(Lexemes.LexemeInt).asInstanceOf[Int]
                this.parseToken(Lexemes.LexemeCloseParen)

                size
            case _ => 0
        }

        val initValue = varType match {
            case _ if this.lexemeIs(Lexemes.LexemeAssign) =>
                this.parseToken(Lexemes.LexemeAssign)
                this.parseExpression()
            case "Int"   => 0
            case "Bool"  => false
            case "Array" => List.fill(arraySize)(0)
            case _       =>
                throw new RuntimeException("parser error: unsupported variable type %s" % varType)
        }

        this.ir.appendVarInfo(identName, varType, initValue)
    }


    def parseStatementsStart(): Block = {
        if (this.isTabOf(this.curTabNum + 1)) {
            this.curTabNum += 1
            val stmts = this.parseStatement()
            val block = this.parseStatements()
            this.curTabNum -= 1

            stmts.foreach(block.prependStmt)

            block
        } else {
            new Block()
        }
    }


    def parseStatements(): Block = {
        if (this.isTabOf(this.curTabNum)) {
            val stmts = this.parseStatement()
            val block = this.parseStatements()

            stmts.foreach(block.prependStmt)

            block
        } else {
            new Block()
        }
    }


    def parseStatement(): List[Statement] = {  /* List because of for-loop parsing into assign and while statements */
        this.parseToken(Lexemes.LexemeTab)

        if (this.lexemeIs(Lexemes.LexemeIdent)) {
            this.parseAssignment() :: Nil
        } else if (this.lexemeIs(Lexemes.LexemeIf)) {
            this.parseCondition() :: Nil
        } else if (this.lexemeIs(Lexemes.LexemeWhile)) {
            this.parseWhileLoop() :: Nil
        } else if (this.lexemeIs(Lexemes.LexemeFor)) {
            val pair = this.parseForLoop()
            pair._2 :: pair._1 :: Nil
        } else {
            this.setError("invalid token %s (expected lexeme with ID %d)" % (this.getToken.toString(), Lexemes.LexemeTab))
        }
    }


    def parseAssignment(): Assign = {
        val access = this.parseAccess()
        this.parseToken(Lexemes.LexemeAssign)
        val expr = this.parseExpression()

        new Assign(access, expr)
    }


    def parseCondition(): IfLike = {
        this.parseToken(Lexemes.LexemeIf)
        val cond = this.parseExpression()
        this.parseToken(Lexemes.LexemeColon)
        val block = this.parseStatementsStart()

        /* Else-block */

        if (this.isTabOf(this.curTabNum) && this.tokens(this.pos + 1).lexemeId == Lexemes.LexemeElse) {
            this.parseToken(Lexemes.LexemeTab)
            this.parseToken(Lexemes.LexemeElse)
            this.parseToken(Lexemes.LexemeColon)

            val elseBlock = this.parseStatementsStart()

            new IfElse(cond, block, elseBlock)
        } else {
            new If(cond, block)
        }
    }


    def parseWhileLoop(): While = {
        this.parseToken(Lexemes.LexemeWhile)
        val cond = this.parseExpression()
        this.parseToken(Lexemes.LexemeColon)
        val block = this.parseStatementsStart()

        new While(cond, block)
    }


    def parseForLoop(): (Assign, While) = {  /* for i = 1 to 10: */
        this.parseToken(Lexemes.LexemeFor)
        val identName = this.parseToken(Lexemes.LexemeIdent).asInstanceOf[String]
        this.parseToken(Lexemes.LexemeAssign)

        /* Start value */

        var startValue: Int = 0

        if (this.lexemeIs(Lexemes.LexemeMinus)) {
            this.parseToken(Lexemes.LexemeMinus)
            startValue = -this.parseToken(Lexemes.LexemeInt).asInstanceOf[Int]
        } else {
            startValue = this.parseToken(Lexemes.LexemeInt).asInstanceOf[Int]
        }

        this.parseToken(Lexemes.LexemeTo)

        /* End value */

        var endValue: Int = 0

        if (this.lexemeIs(Lexemes.LexemeMinus)) {
            this.parseToken(Lexemes.LexemeMinus)
            endValue = -this.parseToken(Lexemes.LexemeInt).asInstanceOf[Int]
        } else {
            endValue = this.parseToken(Lexemes.LexemeInt).asInstanceOf[Int]
        }

        this.parseToken(Lexemes.LexemeColon)

        /* --- */

        val block = this.parseStatementsStart()

        val ident      = new Ident(identName)
        val assign     = new Assign(ident, new Const(startValue))
        val compExpr   = if (startValue <= endValue)
            new ELT(ident, new Const(endValue)) else
            new EGT(ident, new Const(endValue))
        val loopAssign = new Assign(ident, if (startValue <= endValue)
            new Sum(ident, new Const(1)) else
            new Dif(ident, new Const(1)))
        val loop = new While(compExpr, block.appendStmt(loopAssign))

        (assign, loop)
    }


    def parseAccess(): AccessLike = {
        val identName = this.parseToken(Lexemes.LexemeIdent).asInstanceOf[String]

        this.bufAccessCnt = 0

        if (this.lexemeIs(Lexemes.LexemeOpenBrace)) {
            val optAccess = this.parseAccessOpt()

            optAccess match {
                case Some(access) => access.setIdent(identName)
                case None         => new Access(identName)
            }
        } else {
            new Ident(identName)
        }
    }


    def parseAccessOpt(): Option[Access] = {
        if (this.lexemeIs(Lexemes.LexemeOpenBrace)) {
            this.parseToken(Lexemes.LexemeOpenBrace)

            val expr = this.parseExpression()

            this.bufAccessCnt += 1

            this.parseToken(Lexemes.LexemeCloseBrace)
            val optAccess = this.parseAccessOpt()

            val access = optAccess match {
                case Some(acc) => acc
                case None      => new Access()
            }

            Some(access.prependExpr(expr))
        } else {
            None
        }
    }


    def parseExpression(): Expr = this.parseExpr1()


    def parseExpr1(): Expr = {
        val expr     = this.parseExpr2()
        val nextExpr = this.parseOptExpr1(expr)

        nextExpr
    }


    def parseOptExpr1(leftExpr: Expr): Expr = {
        if (this.lexemeIs(Lexemes.LexemeAnd) || this.lexemeIs(Lexemes.LexemeOr) || this.lexemeIs(Lexemes.LexemeXor)) {
            val binExpr =
                if (this.lexemeIs(Lexemes.LexemeAnd)) {
                    this.parseToken(Lexemes.LexemeAnd)
                    new And(leftExpr, null)
                } else if (this.lexemeIs(Lexemes.LexemeOr)) {
                    this.parseToken(Lexemes.LexemeOr)
                    new Or(leftExpr, null)
                } else if (this.lexemeIs(Lexemes.LexemeXor)) {
                    this.parseToken(Lexemes.LexemeXor)
                    new Xor(leftExpr, null)
                } else {
                    throw new RuntimeException("unreachable statement")
                }

            val expr     = this.parseExpr2()
            val nextExpr = this.parseOptExpr1(expr)

            binExpr.right = nextExpr

            binExpr
        } else {
            leftExpr
        }
    }


    def parseExpr2(): Expr = {
        val expr     = this.parseExpr3()
        val nextExpr = this.parseOptExpr2(expr)

        nextExpr
    }


    def parseOptExpr2(leftExpr: Expr): Expr = {
        if (this.lexemeIs(Lexemes.LexemeEq) ||
            this.lexemeIs(Lexemes.LexemeLt) ||
            this.lexemeIs(Lexemes.LexemeGt) ||
            this.lexemeIs(Lexemes.LexemeNeq) ||
            this.lexemeIs(Lexemes.LexemeElt) ||
            this.lexemeIs(Lexemes.LexemeEgt))
        {
            val binExpr =
                if (this.lexemeIs(Lexemes.LexemeEq)) {
                    this.parseToken(Lexemes.LexemeEq)
                    new Eq(leftExpr, null)
                } else if (this.lexemeIs(Lexemes.LexemeLt)) {
                    this.parseToken(Lexemes.LexemeLt)
                    new LT(leftExpr, null)
                } else if (this.lexemeIs(Lexemes.LexemeGt)) {
                    this.parseToken(Lexemes.LexemeGt)
                    new GT(leftExpr, null)
                } else if (this.lexemeIs(Lexemes.LexemeNeq)) {
                    this.parseToken(Lexemes.LexemeNeq)
                    new NEq(leftExpr, null)
                } else if (this.lexemeIs(Lexemes.LexemeElt)) {
                    this.parseToken(Lexemes.LexemeElt)
                    new ELT(leftExpr, null)
                } else if (this.lexemeIs(Lexemes.LexemeEgt)) {
                    this.parseToken(Lexemes.LexemeEgt)
                    new EGT(leftExpr, null)
                } else {
                    throw new RuntimeException("unreachable statement")
                }

            val expr     = this.parseExpr2()
            val nextExpr = this.parseOptExpr1(expr)

            binExpr.right = nextExpr

            binExpr
        } else {
            leftExpr
        }
    }


    def parseExpr3(): Expr = {
        val expr     = this.parseExpr4()
        val nextExpr = this.parseOptExpr3(expr)

        nextExpr
    }


    def parseOptExpr3(leftExpr: Expr): Expr = {
        if (this.lexemeIs(Lexemes.LexemeMul) || this.lexemeIs(Lexemes.LexemeDiv) || this.lexemeIs(Lexemes.LexemeMod)) {
            val binExpr =
                if (this.lexemeIs(Lexemes.LexemeMul)) {
                    this.parseToken(Lexemes.LexemeMul)
                    new Mul(leftExpr, null)
                } else if (this.lexemeIs(Lexemes.LexemeDiv)) {
                    this.parseToken(Lexemes.LexemeDiv)
                    new Div(leftExpr, null)
                } else if (this.lexemeIs(Lexemes.LexemeMod)) {
                    this.parseToken(Lexemes.LexemeMod)
                    new Mod(leftExpr, null)
                } else {
                    throw new RuntimeException("unreachable statement")
                }

            val expr     = this.parseExpr2()
            val nextExpr = this.parseOptExpr1(expr)

            binExpr.right = nextExpr

            binExpr
        } else {
            leftExpr
        }
    }


    def parseExpr4(): Expr = {
        val expr     = this.parseExpr5()
        val nextExpr = this.parseOptExpr4(expr)

        nextExpr
    }


    def parseOptExpr4(leftExpr: Expr): Expr = {
        if (this.lexemeIs(Lexemes.LexemePlus) || this.lexemeIs(Lexemes.LexemeMinus)) {
            val binExpr =
                if (this.lexemeIs(Lexemes.LexemePlus)) {
                    this.parseToken(Lexemes.LexemePlus)
                    new Sum(leftExpr, null)
                } else if (this.lexemeIs(Lexemes.LexemeMinus)) {
                    this.parseToken(Lexemes.LexemeMinus)
                    new Dif(leftExpr, null)
                } else {
                    throw new RuntimeException("unreachable statement")
                }

            val expr     = this.parseExpr2()
            val nextExpr = this.parseOptExpr1(expr)

            binExpr.right = nextExpr

            binExpr
        } else {
            leftExpr
        }
    }


    def parseExpr5(): Expr = {
        if (this.lexemeIs(Lexemes.LexemeInt)) {
            val num = this.parseToken(Lexemes.LexemeInt).asInstanceOf[Int]
            new Const(num)
        } else if (this.lexemeIs(Lexemes.LexemeBool)) {
            val flag = this.parseToken(Lexemes.LexemeBool).asInstanceOf[Boolean]
            new Const(flag)
        } else if (this.lexemeIs(Lexemes.LexemeIdent)) {
            this.parseAccess()
        } else if (this.lexemeIs(Lexemes.LexemeOpenParen)) {
            this.parseToken(Lexemes.LexemeOpenParen)
            val expr = this.parseExpression()
            this.parseToken(Lexemes.LexemeCloseParen)

            expr
        } else if (this.lexemeIs(Lexemes.LexemeMinus)) {
            this.parseToken(Lexemes.LexemeMinus)
            val expr = this.parseExpression()

            new UnaryMinus(expr)
        } else if (this.lexemeIs(Lexemes.LexemeNot)) {
            this.parseToken(Lexemes.LexemeNot)
            val expr = this.parseExpression()

            new Not(expr)
        } else {
            this.setError("invalid token %s (expected lexeme with ID %d, %d, %d, %d, %d or %d)" % (
                this.getToken.toString,
                Lexemes.LexemeInt,
                Lexemes.LexemeBool,
                Lexemes.LexemeIdent,
                Lexemes.LexemeOpenParen,
                Lexemes.LexemeMinus,
                Lexemes.LexemeNot
            ))
        }
    }

}

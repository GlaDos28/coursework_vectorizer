# -*- coding: utf-8 -*-


import scanning.Lexer as Lexer
import parsing.Parser as Parser


def translate(text):
    return Parser.Parser(Lexer.Lexer(text).get_processed_tokens()).parse()

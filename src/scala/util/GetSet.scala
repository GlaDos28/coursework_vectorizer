package scala.util

trait GetSet[T] {
    val get: ()  => T
    val set: (T) => Unit
}

object GetSet {
    def by[T](getter: () => T, setter: (T) => Unit): GetSet[T] = new GetSet[T] {
        val get: ()  => T    = getter
        val set: (T) => Unit = setter
    }
}
package scala.util

class OperationNotAllowedException(msg: String) extends RuntimeException(msg) {
    def this() = this(OperationNotAllowedException.DefaultMsg)
}

object OperationNotAllowedException {
    val DefaultMsg = "operation is now allowed"
}
package scala.util

case class Counter(var n: Int = 0) {
    def next: Int = {
        n += 1
        n - 1
    }
}

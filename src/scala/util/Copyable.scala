package scala.util

trait Copyable[T] {
    def copy: T
}

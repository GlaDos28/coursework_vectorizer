package scala.util.domTree

import com.liangdp.graphviz4s.Digraph

import scala.backend.cfg.{Block, CFG}
import scala.util.methods.stringEscaped
import scala.collection.mutable.{
    ListBuffer,
    HashMap => MHashMap,
    Map     => MMap,
    HashSet => MHashSet
}

class DomTree private(
    val sdom:  MHashMap[Block, ListBuffer[Block]],
    val spdom: MHashMap[Block, ListBuffer[Block]]) {

    def this() = this(MHashMap(), MHashMap())

    def domToPdf(filename: String, view: Boolean = false): Unit = {
        domDataToPdf(filename, sdom, "Dominator tree", view)
    }

    def pdomToPdf(filename: String, view: Boolean = false): Unit = {
        domDataToPdf(filename, spdom, "Postdominator tree", view)
    }

    private def domDataToPdf(
        filename: String,
        domMap: MHashMap[Block, ListBuffer[Block]],
        graphName: String,
        view: Boolean): Unit = {

        val dot = new Digraph(comment = graphName)

        /* Set block IDs and add nodes */

        var idCounter = 0
        val idMap = MHashMap[Block, Int]()

        domMap.foreach(pair => if (!idMap.contains(pair._1)) {
            idMap += (pair._1 -> idCounter)
            idCounter += 1

            dot.node(idMap(pair._1).toString, pair._1 match {
                case block if block.prevIter().isEmpty => "INPUT"
                case block if block.nextIter().isEmpty => "OUTPUT"
                case _ => pair._1.statements.map(_.toString.escaped).reduceLeft(_ + "\n" + _)
            }, attrs = MMap("shape" -> "oval"))
        })

        /* Put edges */

        domMap.foreach(pair => if (pair._2.nonEmpty)
            dot.edge(idMap(pair._2.head).toString, idMap(pair._1).toString))

        /* Output tree */

        dot.render(fileName = filename, directory = "resrc/test/res", cleanUp = true, view = view)
    }
}

object DomTree {
    def fromCFG(cfg: CFG): DomTree = {
        new DomTree(
            buildSDomData(cfg.blocks, _.prevIter(), cfg.inputBlock,  cfg.outputBlock),
            buildSDomData(cfg.blocks, _.nextIter(), cfg.outputBlock, cfg.inputBlock)
        )
    }

    def buildSDomData(
        blocks: ListBuffer[Block],
        blockAccessor: Block => Iterator[Block],
        startBlock: Block,
        endBlock:   Block): MHashMap[Block, ListBuffer[Block]] = {

        val mapDom = MHashMap[Block, MHashSet[Block]]()

        /* Initialize empty dom sets */

        blocks.foreach(block => mapDom.put(block, MHashSet()))
        mapDom.put(startBlock, MHashSet())
        mapDom.put(endBlock,   MHashSet())

        /* Initialize start block */

        mapDom(startBlock).add(startBlock)

        /* For every node except start block set every node as dominator */

        (blocks ++ List(endBlock)).foreach(block1 => {
            blocks.foreach(block2 => mapDom(block1).add(block2))
            mapDom(block1).add(startBlock)
            mapDom(block1).add(endBlock)
        })

        /* Iteratively remove false dominators */

        var stop = false

        while (!stop) {
            stop = true

            (blocks ++ List(endBlock)).foreach(block => {
                val intersec = blockAccessor(block) map(mapDom(_)) reduceLeft(_ intersect _)
                val newDomSet = intersec + block

                if (newDomSet != mapDom(block)) {
                    stop = false
                    mapDom.update(block, newDomSet)
                }
            })
        }

        /* Transfer results to the dominator tree */

        val res: MHashMap[Block, ListBuffer[Block]] = MHashMap()

        mapDom.foreach(pair => res.put(pair._1, pair._2
            .to[ListBuffer]
            .sortWith((b1, b2) => mapDom(b1) contains b2) /* Sort by ascending dominance */
            .drop(1)))                                    /* Remove improper dominator   */

        res
    }
}

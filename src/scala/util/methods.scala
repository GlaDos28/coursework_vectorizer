package scala.util

import scala.backend.codeText.CodeDump

object methods {
    /*implicit def seqInsertBetweenIf[T](lst: Seq[T]): {
        def insertBetweenIf(pred: (T, T) => Boolean, ins: T): Seq[T]
    } = new {
        def insertBetweenIf(pred: (T, T) => Boolean, ins: T): Seq[T] = lst match {
            case Nil                        => Nil
            case x :: y :: xs if pred(x, y) => x :: ins :: seqInsertBetweenIf[T](y :: xs).insertBetweenIf(pred, ins).toList
            case x :: xs                    => x :: seqInsertBetweenIf[T](xs).insertBetweenIf(pred, ins).toList
        }
    } TODO complete */

    implicit def seqInsertBetweenIf(lst: Seq[CodeDump]): {
        def insertBetweenIf(pred: (CodeDump, CodeDump) => Boolean, ins: CodeDump): Seq[CodeDump]
    } = new {
        def insertBetweenIf(pred: (CodeDump, CodeDump) => Boolean, ins: CodeDump): Seq[CodeDump] = lst match {
            case Nil                        => Nil
            case x :: y :: xs if pred(x, y) => x :: ins :: seqInsertBetweenIf(y :: xs).insertBetweenIf(pred, ins).toList
            case x :: xs                    => x :: seqInsertBetweenIf(xs).insertBetweenIf(pred, ins).toList
            case xs                         => xs
        }
    }

    implicit def intTimes(n: Int): {
        def times(f: () => Unit): Unit
    } = new {
        def times(f: () => Unit): Unit = (1 to n) foreach (_ => f())
    }

    implicit def stringEscaped(s: String): {
        def escaped: String
    } = new {
        def escaped: String = s
            .replace("\\", "\\\\")
            .replace("\"", "\\\"")
    }
}

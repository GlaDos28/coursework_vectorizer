package scala.util

case class ExtendedInt(value: Int) {
    def toSubscript: String = value.toString.map({
        case '0' => '₀'
        case '1' => '₁'
        case '2' => '₂'
        case '3' => '₃'
        case '4' => '₄'
        case '5' => '₅'
        case '6' => '₆'
        case '7' => '₇'
        case '8' => '₈'
        case '9' => '₉'
        case  ch  =>  ch
    })
}

object ExtendedInt {
    implicit def toExtendedInt(x: Int): ExtendedInt = ExtendedInt(x)
}
package scala.backend.codeText

class CodeDump private(val str: Option[String], val block: List[CodeDump], val withOffset: Boolean) {
    def this(block: List[CodeDump], withOffset: Boolean = true) = this(None, block, withOffset)

    def getText(identNum: Int = 0, identStr: String = CodeDump.DEFAULT_IDENT): String = {
        str match {
            case Some(strValue) => strValue
            case None           => getTextRec(identNum, identStr)
        }
    }

    private def getTextRec(identNum: Int, identStr: String): String = {
        val strOffset  = identStr * (identNum - 2)
        val listOffset = identStr * identNum

        (str, block) match {
            case (Some(strValue), _) => strOffset  + strValue
            case (None, Nil)         => listOffset + "/* %s */".format(CodeDump.COMMENT_EMPTY)
            case (None, _)           => block
                .map(_.getTextRec(if (withOffset) identNum + 1 else identNum, identStr))
                .reduceLeft(_ + "\n" + _)
        }
    }

    override def toString: String = getText()
}

object CodeDump {
    val DEFAULT_IDENT = "    "
    val COMMENT_EMPTY = "empty"

    implicit def stringToCodeDump(str: String): CodeDump = new CodeDump(Some(str), Nil, true)
}
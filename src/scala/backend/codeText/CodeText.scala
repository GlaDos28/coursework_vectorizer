package scala.backend.codeText


import java.io.{File, FileNotFoundException, PrintWriter}
import java.nio.file.FileSystems

import scala.backend.ir.IR
import scala.frontend.Translator
import scala.io.Source


class CodeText(val text: String) {

    def toFile(filename: String): Unit = {
        val pw = new PrintWriter(new File(filename))
        pw.write(text)
        pw.close()
    }


    private[backend] def ir: IR = {
        Translator.translate(this)
    }


    override def toString: String = text

}

object CodeText {

    def fromFile(filePath: String): CodeText = {
        try {
            val text = Source
                .fromFile(filePath)
                .getLines.toArray
                .reduceLeft(_ + "\n" + _)

            new CodeText(text)
        } catch {
            case _: FileNotFoundException =>
                val absPath = FileSystems.getDefault.getPath(filePath).normalize.toAbsolutePath.toString
                throw new FileNotFoundException("File \"%s\" is not found".format(absPath))
        }
    }

}

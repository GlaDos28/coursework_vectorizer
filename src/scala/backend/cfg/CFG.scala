package scala.backend.cfg

import scala.util.{Copyable, GetSet}
import scala.backend.ir.{IR, NodeType}
import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer, HashMap => MHashMap, Stack => MStack}
import backend.ir.nodes.{Block => IRBlock, _}
import com.liangdp.graphviz4s.Digraph

import scala.util.methods.stringEscaped
import scala.backend.codeText.CodeDump
import scala.backend.ct.ControlTree
import scala.util.domTree.DomTree

class CFG private[backend](
    var varBlock:    VarBlock          = new VarBlock(),
    var blocks:      ListBuffer[Block] = ListBuffer(),
    var inputBlock:  Block             = new Block(),
    var outputBlock: Block             = new Block()) extends Copyable[CFG] {

    var domTree: Option[DomTree] = None

    def buildDomTree(): Unit = {
        domTree = Some(DomTree.fromCFG(this))

        blocks.foreach(block => block.domTree = domTree)

        inputBlock.domTree  = domTree
        outputBlock.domTree = domTree
    }

    def withSSA: CFG with SSA = {
        val res = new CFG(varBlock.copy) with SSA
        CFG.fillCopy(this, res)

        res.buildDomTree()
        res.allVarNames.toSeq.sortWith(_ > _).foreach(res.injectSSAForVar)
        res.injectSSAVersions()

        res
    }

    def simplified: CFG = {
        val res = new CFG(varBlock.copy)
        CFG.fillCopy(this, res)

        res.blocks.foreach(block => if (block.statements.isEmpty) { /* has defined n1 branch */
            val next = block.n1.get
            next.ps -= block

            block.ps.foreach(prev => {
                if (prev.n1.isDefined && prev.n1.get == block) {
                    prev.n1 = Some(next)
                } else {
                    prev.n2 = Some(next)
                }

                next.ps += prev
            })
        })

        res.blocks = res.blocks.filter(_.statements.nonEmpty)

        res
    }

    def controlTree: ControlTree = ControlTree.fromCFG(this)

    def reifyIR: IR = {
        controlTree.reifyIR
    }

    def dumpBlockInfo(): Unit = {
        var idCounter = 0
        val idMap = MHashMap[Block, Int]()

        blocks.foreach((block) => {
            idMap += (block -> idCounter)
            idCounter += 1
        })

        idMap += (inputBlock -> -1)
        idMap += (outputBlock -> -2)

        val iter = blocks.iterator

        while (iter.hasNext) {
            val block = iter.next()

            println("Block %d:".format(idMap(block)))

            if (block.ps.nonEmpty) {
                println("\t* ps: %s".format(block.ps.map(idMap(_).toString).reduceLeft(_ + ", " + _)))
            }

            if (block.n1.isDefined) {
                if (block.n2.isDefined) {
                    println("\t* n1: %d, n2: %d".format(idMap(block.n1.get), idMap(block.n2.get)))
                } else {
                    println("\t* n1: %d".format(idMap(block.n1.get)))
                }
            } else if (block.n2.isDefined) {
                println("\t* n2: %d".format(idMap(block.n2.get)))
            }

            block.statements.foreach(statement => println("\t" + statement))
        }
    }

    def toPdf(filename: String, backwardEdges: Boolean = false, view: Boolean = false): Unit = {
        val dot = new Digraph(comment = "Control flow graph")

        /* Set block IDs */

        var idCounter = 0
        val idMap = MHashMap[Block, Int]()

        blocks.foreach((block) => {
            idMap += (block -> idCounter)
            idCounter += 1
        })

        idMap += (inputBlock -> (idCounter + 1))
        idMap += (outputBlock -> (idCounter + 2))

        /* Put nodes */

        blocks.foreach(block => dot.node(idMap(block).toString.escaped, block.toString.escaped, attrs = mutable.Map("shape" -> "box")))

        dot.node(idMap(inputBlock).toString,  "START", attrs = mutable.Map("shape" -> "box"))
        dot.node(idMap(outputBlock).toString, "END",   attrs = mutable.Map("shape" -> "box"))

        /* Put edges */

        blocks.foreach(block => {
            if (block.n1.isDefined) {
                dot.edge(idMap(block).toString, idMap(block.n1.get).toString, if (block.n2.isDefined) "T" else null)
            }

            if (block.n2.isDefined) {
                dot.edge(idMap(block).toString, idMap(block.n2.get).toString, if (block.n1.isDefined) "F" else null)
            }

            if (backwardEdges) {
                block.ps.foreach(prev =>
                    dot.edge(idMap(block).toString, idMap(prev).toString, attrs = mutable.Map("style" -> "dashed")))
            }
        })

        dot.edge(idMap(inputBlock).toString, idMap(inputBlock.n1.get).toString)

        if (backwardEdges) {
            outputBlock.ps.foreach(prev =>
                dot.edge(idMap(outputBlock).toString, idMap(prev).toString, attrs = mutable.Map("style" -> "dashed")))
        }

        /* Output CFG */

        dot.render(fileName = filename, directory = "resrc/test/res", cleanUp = true, view = view)
    }

    override def copy: CFG = {
        val res = new CFG(varBlock.copy)
        CFG.fillCopy(this, res)
        res
    }

    override def toString: String = reifyIR.toString

    private def evalBackEdges(clearFirst: Boolean = true): Unit = {
        if (clearFirst) {
            blocks.foreach(block => block.ps.clear())
            outputBlock.ps.clear()
        }

        blocks.foreach(block => block.nextIter().foreach(next => next.ps += block))
        inputBlock.n1.get.ps += inputBlock
    }

    /* Specific methods for SSA evaluation */

    private def allVarNames: Set[String] = {
        blocks
            .map(_.statements
                .filter(stmt =>
                    stmt.nodeType == NodeType.Assign
                    && stmt.asInstanceOf[Assign].withIdent)
                .map(_.asInstanceOf[Assign].lhv.asInstanceOf[Ident].name))
            .reduceLeft(_ union _)
            .toSet
    }

    private def getVarAssignBlocks(varName: String): Set[Block] = {
        blocks
            .filter(_.statements.exists(stmt =>
                stmt.nodeType == NodeType.Assign
                && stmt.asInstanceOf[Assign].withIdent
                && stmt.asInstanceOf[Assign].lhv.asInstanceOf[Ident].name == varName))
            .toSet
    }

    private def calculateDF(blocSet: Set[Block]): Set[Block] = {
        var curDF  = blocSet
        var newDF  = blocSet
        var pureDF = Set[Block]()

        do {
            pureDF = curDF
                .map(block => blocks.filter(someBlock =>
                    !(block sdom someBlock) && someBlock.ps.exists(block dom)))
                .reduceLeft(_ union _)
                .toSet

            curDF = newDF
            newDF = curDF union pureDF
        } while(curDF != newDF)

        pureDF
    }

    private def injectSSAForVar(varName: String): Unit = {
        val df = calculateDF(getVarAssignBlocks(varName))

        df.foreach(block => {
            val phi = new Phi(new Ident(varName), ArrayBuffer.fill(block.ps.length)(0))

            block.statements.prepend(phi)
        })
    }

    private def injectSSAVersions(
        block: Block = inputBlock,
        varVersions: MHashMap[String, (Int, MStack[Int])] = MHashMap()): Unit = {

        block.stmtIter[HasValue].foreach(stmt => {
            stmt match {
                case s: HasRHV => s.rhvIdents.foreach(getSet =>
                    getSet.set(new VersionedIdent(getSet.get().name, varVersions(getSet.get().name)._2.top)))
                case _ =>
            }

            stmt match {
                case s: HasLHV => CFG.putVersion(varVersions, s.lhvIdent)
                case _ =>
            }
        })

        block.nextIter().foreach(next => {
            val edgeIndex = next.ps.indexOf(block)

            next.stmtIter[Phi].foreach(phi => {
                val stack = varVersions.getOrElseUpdate(phi.ident.name, (0, new MStack[Int]()))._2
                phi.indices(edgeIndex) = if (stack.nonEmpty) stack.top else 0
            })
        })

        block.domChildren
            .toList
            .sortWith((b, _) => b.ps.contains(block))
            .foreach(injectSSAVersions(_, varVersions))
        block.stmtIter[HasLHV].filter(_.withIdent).foreach(stmt => varVersions(stmt.lhvIdent.get().name)._2.pop())
    }
}

object CFG {
    def fromIR(ir: IR): CFG = {
        val cfg = traverseIRBlock(ir.root.block, new Block())
        val oldInput = cfg.inputBlock

        cfg.blocks += oldInput
        cfg.inputBlock = new Block(n1 = Some(oldInput))

        cfg.evalBackEdges(clearFirst = false)
        cfg.varBlock = ir.root.varBlock.copy

        cfg
    }

    private def traverseIRBlock(irBlock: IRBlock, output: Block): CFG = {
        if (irBlock.statements.isEmpty) {
            val emptyBlock = new Block(ListBuffer(new Comment(CodeDump.COMMENT_EMPTY)), n1 = Some(output))
            return new CFG(new VarBlock(), ListBuffer(), emptyBlock, output)
        }

        val subCfgs = ListBuffer[CFG]()
        val iter    = irBlock.statements.reverseIterator.buffered

        var curOutput = new Block(n1 = Some(output))

        while (iter.hasNext) {
            val startStmt = iter.next()

            startStmt match {
                case _: Assign =>
                    val stmts = ListBuffer[Node](startStmt)

                    while (iter.hasNext && iter.head.isInstanceOf[Assign]) {
                        stmts += iter.next()
                    }

                    val block = new Block(stmts.reverse, n1 = Some(curOutput))

                    subCfgs += new CFG(new VarBlock(), ListBuffer(), block, curOutput)

                    curOutput = block
                case _: If =>
                    val stmt      = startStmt.asInstanceOf[If]
                    val condBlock = new Block(ListBuffer(stmt.condition))
                    val bodyCfg   = traverseIRBlock(stmt.body, curOutput)

                    condBlock.n1 = Some(bodyCfg.inputBlock)
                    condBlock.n2 = Some(curOutput)

                    bodyCfg.blocks    += bodyCfg.inputBlock
                    bodyCfg.inputBlock = condBlock

                    subCfgs += bodyCfg

                    curOutput = condBlock
                case _: IfElse =>
                    val stmt      = startStmt.asInstanceOf[IfElse]
                    val condBlock = new Block(ListBuffer(stmt.condition))
                    val tBodyCfg  = traverseIRBlock(stmt.tBody, curOutput)
                    val fBodyCfg  = traverseIRBlock(stmt.fBody, curOutput)

                    condBlock.n1 = Some(tBodyCfg.inputBlock)
                    condBlock.n2 = Some(fBodyCfg.inputBlock)

                    val subCfg = new CFG(new VarBlock(), tBodyCfg.blocks ++ fBodyCfg.blocks ++ ListBuffer(tBodyCfg.inputBlock, fBodyCfg.inputBlock), condBlock, curOutput)

                    subCfgs += subCfg

                    curOutput = condBlock
                case _: While =>
                    val stmt      = startStmt.asInstanceOf[While]
                    val condBlock = new Block(ListBuffer(stmt.condition))
                    val bodyCfg   = traverseIRBlock(stmt.body, condBlock)

                    condBlock.n1 = Some(bodyCfg.inputBlock)
                    condBlock.n2 = Some(curOutput)

                    bodyCfg.blocks     += bodyCfg.inputBlock
                    bodyCfg.inputBlock  = condBlock
                    bodyCfg.outputBlock = curOutput

                    subCfgs += bodyCfg

                    curOutput = condBlock
            }
        }

        val blocks: ListBuffer[Block] = subCfgs
            .map((subCfg) => ListBuffer(subCfg.outputBlock) ++ subCfg.blocks)
            .reduceLeft(_ ++ _)

        new CFG(new VarBlock(), blocks, curOutput, output)
    }

    private def putVersion(varVersions: MHashMap[String, (Int, MStack[Int])], ident: GetSet[Ident]): Unit = {
        var (version, stack) = varVersions.getOrElseUpdate(ident.get().name, (0, new MStack[Int]()))

        version += 1
        stack push version

        ident.set(new VersionedIdent(ident.get().name, version))

        varVersions.update(ident.get().name, (version, stack))
    }

    private def fillCopy(oldCfg: CFG, newCfg: CFG): Unit = {
        newCfg.domTree = None

        val map = new MHashMap[Block, Block]()

        newCfg.inputBlock  = new Block(oldCfg.inputBlock.statements.map(_.copy))
        newCfg.outputBlock = new Block(oldCfg.outputBlock.statements.map(_.copy))

        oldCfg.blocks.foreach(block => {
            val copy = new Block(block.statements.map(_.copy))
            newCfg.blocks += copy
            map += (block -> copy)
        })

        map += (oldCfg.inputBlock  -> newCfg.inputBlock)
        map += (oldCfg.outputBlock -> newCfg.outputBlock)

        oldCfg.blocks.foreach(block => {
            val newBlock = map(block)

            block.ps.foreach(p => newBlock.ps += map(p))

            if (block.n1.isDefined) { newBlock.n1 = Some(map(block.n1.get)) }
            if (block.n2.isDefined) { newBlock.n2 = Some(map(block.n2.get)) }
        })

        newCfg.inputBlock.n1 = Some(map(oldCfg.inputBlock.n1.get))
        oldCfg.outputBlock.ps.foreach(p => newCfg.outputBlock.ps += map(p))
    }
}

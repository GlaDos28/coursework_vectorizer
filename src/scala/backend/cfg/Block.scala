package scala.backend.cfg

import scala.backend.ir.nodes.Node
import scala.collection.mutable.ListBuffer
import scala.util.OperationNotAllowedException
import scala.util.domTree.DomTree

class Block(
    var statements: ListBuffer[Node]  = ListBuffer(), /* block statements */
    var ps:         ListBuffer[Block] = ListBuffer(), /* previous blocks  */
    var n1:         Option[Block]     = None,         /* next block 1     */
    var n2:         Option[Block]     = None,         /* next block 2     */
    var domTree:    Option[DomTree]   = None) {

    def prevIter(): Iterator[Block] = ps.iterator
    def nextIter(): Iterator[Block] = List(n1, n2).filter(_.isDefined).map(_.get).iterator

    def stmtIter[T <: Node: Manifest]: Iterator[T] = statements.collect {
        case stmt if manifest[T].runtimeClass.isInstance(stmt) => stmt.asInstanceOf[T]
    }.iterator

    def dom  (that: Block): Boolean = (this sdom that) || (this == that)
    def sdom (that: Block): Boolean = ensureDomBuilt().sdom(that).contains(this)
    def idom (that: Block): Boolean = {
        val lst = ensureDomBuilt().sdom(that)
        lst.nonEmpty && lst.head == this
    }
    def pdom (that: Block): Boolean = (this spdom that) || (this == that)
    def spdom(that: Block): Boolean = ensureDomBuilt().spdom(that).contains(this)
    def ipdom(that: Block): Boolean = {
        val lst = ensureDomBuilt().spdom(that)
        lst.nonEmpty && lst.head == this
    }

    def domChildren:  Iterator[Block] = ensureDomBuilt().sdom .filter(this idom  _._1).keys.iterator
    def pdomChildren: Iterator[Block] = ensureDomBuilt().spdom.filter(this ipdom _._1).keys.iterator

    def changeNext(oldNext: Block, newNext: Block): Unit = {
        if (n1.isDefined && n1.get == oldNext) {
            n1 = Some(newNext)
        } else {
            n2 = Some(newNext)
        }
    }

    def changePrev(oldPrev: Block, newPrev: Block): Unit = {
        ps -= oldPrev
        ps += newPrev
    }

    def removePrev(oldPrev: Block): Unit = {
        ps -= oldPrev
    }

    override def toString: String = if (statements.isEmpty) "(empty)" else statements
        .map(_.toString)
        .reduceLeft(_ + "\n" + _)

    private def ensureDomBuilt(): DomTree = {
        if (domTree.isEmpty) {
            throw new OperationNotAllowedException(Block.MSG_NO_DOM_TREE)
        }

        domTree.get
    }
}

object Block {
    val MSG_NO_DOM_TREE = "dominator tree is not built"
}

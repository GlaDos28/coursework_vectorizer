package scala.backend.ddg

import com.liangdp.graphviz4s.Digraph

import Preamble._
import scala.backend.ct.ControlTree
import scala.backend.ct.regions._
import scala.backend.ir.nodes.{Block => _, If => _, IfElse => _, While => _, _}
import scala.collection.Set
import scala.collection.mutable.{Map => MMap}

class DDG(val nodes: MMap[Ident, DDGNode] = MMap.empty, val assigns: MMap[Ident, List[Expr]] = MMap.empty) {
    def addNode(ident: Ident): Unit = {
        nodes.getOrElseUpdate(ident, new DDGNode(ident))
    }

    def addDep(lIdent: Ident, rIdent: Ident): Unit = {
        val lNode = nodes.getOrElseUpdate(lIdent, new DDGNode(lIdent))
        val rNode = nodes.getOrElseUpdate(rIdent, new DDGNode(rIdent))

        lNode.inNodes  += (rIdent -> rNode)
        rNode.outNodes += (lIdent -> lNode)
    }

    def addAssign(assign: Assign): Unit = {
        val ident = assign.lhvIdent.get()
        assigns += (ident -> (assign.rhv :: assigns.getOrElseUpdate(ident, Nil)))
    }

    def toPdf(filename: String, view: Boolean = false): Unit = {
        val dot = new Digraph(comment = "Data dependency graph")

        /* Fill graph */

        nodes.values.foreach(node => node.fillDot(dot))

        /* Output tree */

        dot.render(fileName = filename, directory = "resrc/test/res", cleanUp = true, view = view)
    }

    def getInductiveVars: Set[InductiveVar] =
        nodes.flatMap(node => ensureInductivity(node._2)).toSet

    private def ensureInductivity(node: DDGNode): Option[InductiveVar] =
        if (node.outNodes.size == 1 && node.outNodes.head._2.outNodes.exists(n => (n._1.name equals node.ident.name) && n._2.outNodes.size == 1)) {
            val initNode = node
            val phiNode  = initNode.outNodes.head._2
            val stepNode = phiNode.outNodes.find(n => (n._1.name equals node.ident.name) && n._2.outNodes.size == 1).get._2

            /* TODO: check for loop condition to be of the required form */
            if (initNode != phiNode                                                      &&
                initNode != stepNode                                                     &&
                phiNode  != stepNode                                                     &&
                initNode.ident.name.equals(phiNode.ident.name)                           &&
                phiNode.ident.isInstanceOf[VersionedIdent]                               &&
                assigns.getOrElse(initNode.ident, Nil).size == 1                         &&
                assigns(initNode.ident).head.isInstanceOf[Const]                         &&
                assigns(initNode.ident).head.asInstanceOf[Const].value.isInstanceOf[Int] &&
                assigns.getOrElse(stepNode.ident, Nil).size == 1                         &&
                assigns(stepNode.ident).head.isInstanceOf[Sum]                           &&
                assigns(stepNode.ident).head.asInstanceOf[Sum].right.isInstanceOf[Const]) {

                Some(InductiveVar(
                    stepNode.ident.name,
                    phiNode.ident.asInstanceOf[VersionedIdent].version,
                    assigns(initNode.ident).head.asInstanceOf[Const].value.asInstanceOf[Int],
                    assigns(stepNode.ident).head.asInstanceOf[Sum].right.asInstanceOf[Const].value.asInstanceOf[Int]
                ))
            } else {
                None
            }
        } else {
            None
        }
}

object DDG {
    def fromCT(ct: ControlTree): DDG = {
        val ddg = new DDG()

        traverseRegion(ddg, ct.root, Set.empty)

        ddg
    }

    private def traverseRegion(ddg: DDG, region: Region, depIdents: Set[Ident]): Unit = {
        region match {
            case block: Block =>
                block.statements.foreach(stmt => processStatement(ddg, depIdents, stmt))
            case chain: Chain =>
                traverseRegion(ddg, chain.region1, depIdents)
                traverseRegion(ddg, chain.region2, depIdents)
            case ifBlock: If =>
                ifBlock.condRegion.asInstanceOf[Block]
                    .statements.foreach(stmt => processStatement(ddg, depIdents, stmt))

                var newDepIdents = depIdents

                ifBlock.condRegion.asInstanceOf[Block].statements.filter(stmt => stmt.isInstanceOf[HasRHV])
                    .foreach(stmt => stmt.asInstanceOf[HasRHV].rhvIdents
                        .foreach(ident => newDepIdents += ident.get()))

                traverseRegion(ddg, ifBlock.bodyRegion, newDepIdents)
            case ifElseBlock: IfElse =>
                ifElseBlock.condRegion.asInstanceOf[Block]
                    .statements.foreach(stmt => processStatement(ddg, depIdents, stmt))

                var newDepIdents = depIdents

                ifElseBlock.condRegion.asInstanceOf[Block].statements.filter(stmt => stmt.isInstanceOf[HasRHV])
                    .foreach(stmt => stmt.asInstanceOf[HasRHV].rhvIdents
                        .foreach(ident => newDepIdents += ident.get()))

                traverseRegion(ddg, ifElseBlock.fRegion, newDepIdents)
                traverseRegion(ddg, ifElseBlock.tRegion, newDepIdents)
            case whileBlock: While =>
                whileBlock.condRegion.asInstanceOf[Block]
                    .statements.foreach(stmt => processStatement(ddg, depIdents, stmt))

                var newDepIdents = depIdents

                whileBlock.condRegion.asInstanceOf[Block].statements.filter(stmt => stmt.isInstanceOf[HasRHV])
                    .foreach(stmt => stmt.asInstanceOf[HasRHV].rhvIdents
                        .foreach(ident => newDepIdents += ident.get()))

                traverseRegion(ddg, whileBlock.bodyRegion, newDepIdents)
            case _ => throw new RuntimeException("unknown region type: %s" % region.regionType.toString)
        }
    }

    private def processStatement(ddg: DDG, depIdents: Set[Ident], stmt: Node): Unit = stmt match {
        case assign: Assign =>
            var newDepIdents = depIdents

            assign.rhvIdents.foreach(ident => newDepIdents += ident.get())

            ddg.addAssign(assign)
            ddg.addNode(assign.lhvIdent.get())

            newDepIdents.foreach(rIdent => ddg.addDep(assign.lhvIdent.get(), rIdent))
        case phi: Phi =>
            var newDepIdents = depIdents

            phi.indices.foreach(version => newDepIdents += new VersionedIdent(phi.ident.name, version))

            ddg.addNode(phi.ident)
            newDepIdents.foreach(rIdent => ddg.addDep(phi.ident, rIdent))
        case _ =>
    }
}
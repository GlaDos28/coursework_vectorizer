package scala.backend.ddg

import com.liangdp.graphviz4s.Digraph

import scala.backend.ir.nodes.Ident
import scala.collection.mutable.{Map => MMap}

class DDGNode(val ident: Ident, val inNodes: MMap[Ident, DDGNode] = MMap.empty, val outNodes: MMap[Ident, DDGNode] = MMap.empty) {
    protected[backend] def fillDot(dot: Digraph): Unit = {
        dot.node(toString, toString, attrs = MMap("shape" -> "oval"))
        outNodes.keys.foreach(outIdent => dot.edge(toString, outIdent.toString))
    }

    override def toString: String = ident.toString
}

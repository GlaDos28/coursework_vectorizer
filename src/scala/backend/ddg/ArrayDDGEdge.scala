package scala.backend.ddg

class ArrayDDGEdge(val in: ArrayDDGNode, val out: ArrayDDGNode, val d: Int, val anti: Boolean) {
    override def toString: String = if (anti)
        d.toString + " (anti)" else
        d.toString
}

object ArrayDDGEdge {
    def apply(in: ArrayDDGNode, out: ArrayDDGNode, d: Int): ArrayDDGEdge = {
        if (d < 0)
            new ArrayDDGEdge(out, in, -d, true)  else
            new ArrayDDGEdge(in,  out, d, false)
    }
}

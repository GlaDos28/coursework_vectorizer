package scala.backend.ddg

import com.liangdp.graphviz4s.Digraph

import scala.collection.mutable.{Map => MMap}

class SCCArrayDDGNode(val ind: Int, val nodes: Set[ArrayDDGNode], var inEdges: List[SCCArrayDDGEdge] = Nil, var outEdges: List[SCCArrayDDGEdge] = Nil) {
    protected[backend] def fillDot(dot: Digraph): Unit = {
        dot.node(ind.toString, toString, attrs = MMap("shape" -> "oval"))
        outEdges.foreach(e => dot.edge(ind.toString, e.out.ind.toString, e.toString))
    }

    override def toString: String = nodes.mkString("[", ", ", "]")
}

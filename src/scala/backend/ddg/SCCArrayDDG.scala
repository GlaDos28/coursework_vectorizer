package scala.backend.ddg

import com.liangdp.graphviz4s.Digraph

class SCCArrayDDG(val nodes: Array[SCCArrayDDGNode]) {
    def addDep(arrayDDGEdge: ArrayDDGEdge): Unit = {
        val node1 = nodes.find(_.nodes contains arrayDDGEdge.in).get
        val node2 = nodes.find(_.nodes contains arrayDDGEdge.out).get

        if (node1.ind != node2.ind) {
            val edge = new SCCArrayDDGEdge(node1, node2, arrayDDGEdge)

            edge.in.outEdges ::= edge
            edge.out.inEdges ::= edge
        }
    }

    def toPdf(filename: String, view: Boolean = false): Unit = {
        val dot = new Digraph(comment = "Array data dependency graph condensation")

        /* Fill graph */

        nodes.foreach(_.fillDot(dot))

        /* Output tree */

        dot.render(fileName = filename, directory = "resrc/test/res", cleanUp = true, view = view)
    }

    def getLoopsWithStatementIds: List[(List[Int], Boolean)] = { /* TODO: Set -> List here is not trivial, traverse needed */
        getTopologicalOrder.map(node => (node.nodes.toList.map(_.ind), node.nodes.size == 1 && !node.nodes.head.inEdges.exists(e => e.in == e.out && !e.anti && e.d != 0)))
    }

    private def getTopologicalOrder: List[SCCArrayDDGNode] = {
        val traverseNodeArr = new Array[(SCCArrayDDGNode, Int)](nodes.length)

        for (i <- nodes.indices) {
            traverseNodeArr(i) = (nodes(i), 0)
        }

        var counter = 0

        for (i <- nodes.indices) {
            counter = traverse(traverseNodeArr, i, counter)
        }

        traverseNodeArr.sortBy(_._2).toList.map(_._1).reverse
    }

    private def traverse(nodeArr: Array[(SCCArrayDDGNode, Int)], cur: Int, counterValue: Int): Int = {
        var counter = counterValue

        if (nodeArr(cur)._2 == 0) {
            for (edge <- nodeArr(cur)._1.outEdges) {
                counter = traverse(nodeArr, edge.out.ind, counter)
            }

            counter += 1
            nodeArr(cur) = (nodeArr(cur)._1, counter)
        }

        counter
    }
}

object SCCArrayDDG {
    def fromArrayDDG(arrayDDG: ArrayDDG): SCCArrayDDG = {
        var nodes    = arrayDDG.nodes.toList.map[(ArrayDDGNode, Option[Int]), List[(ArrayDDGNode, Option[Int])]]((_, None))
        var quitTime = 0

        nodes.foreach(node => {
            val tmp = dfs1(nodes, node._1, quitTime)

            nodes    = tmp._1
            quitTime = tmp._2
        })

        var sortedNodes = nodes.sortWith(_._2.get > _._2.get).map(_._1)
        var sccNodes    = List[SCCArrayDDGNode]()
        var cnt         = 0

        while (sortedNodes.nonEmpty) {
            val tmp = dfs2(sortedNodes, Set[ArrayDDGNode](), sortedNodes(0))

            sortedNodes = tmp._1
            val ssc     = tmp._2

            sccNodes :+= new SCCArrayDDGNode(cnt, ssc)
            cnt       += 1
        }

        val sccArrayDDG = new SCCArrayDDG(sccNodes.toArray)

        arrayDDG.nodes.flatMap(_.outEdges).foreach(sccArrayDDG.addDep)

        sccArrayDDG
    }

    private def dfs1(nodes: List[(ArrayDDGNode, Option[Int])], node: ArrayDDGNode, quitTime: Int = 0): (List[(ArrayDDGNode, Option[Int])], Int) = {
        var resNodes    = nodes
        var resQuitTime = quitTime

        val nodePair = nodes.find(_._1 == node).get
        val nodeInd  = nodes.indexOf(nodePair)

        if (nodes.find(_._1 == node).get._2.isEmpty) {
            resNodes = resNodes.updated(nodeInd, (node, Some(-1)))

            node.outEdges.foreach(e => {
                val tmp = dfs1(resNodes, e.out, resQuitTime)

                resNodes    = tmp._1
                resQuitTime = tmp._2
            })

            resNodes = resNodes.updated(nodeInd, (node, Some(resQuitTime)))

            (resNodes, resQuitTime + 1)
        } else {
            (resNodes, resQuitTime)
        }
    }

    private def dfs2(nodes: List[ArrayDDGNode], set: Set[ArrayDDGNode], node: ArrayDDGNode): (List[ArrayDDGNode], Set[ArrayDDGNode]) = {
        var resNodes = nodes
        var resSet   = set

        if (nodes.contains(node)) {
            resSet  += node
            resNodes = resNodes.filter(_ != node)

            node.inEdges.foreach(e => {
                val tmp = dfs2(resNodes, resSet, e.in)

                resNodes = tmp._1
                resSet   = tmp._2
            })
        }

        (resNodes, resSet)
    }
}

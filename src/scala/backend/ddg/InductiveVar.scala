package scala.backend.ddg

import scala.backend.ir.nodes.VersionedIdent

case class InductiveVar(identName: String, condIdentVersion: Int, initValue: Int, step: Int) {
    def getCondIdent: VersionedIdent = new VersionedIdent(identName, condIdentVersion)
}

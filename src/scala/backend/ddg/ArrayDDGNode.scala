package scala.backend.ddg

import com.liangdp.graphviz4s.Digraph

import scala.backend.vectorization.ArrayDep
import scala.collection.mutable.{Map => MMap}

class ArrayDDGNode(val ind: Int, val arrayDep: ArrayDep, var inEdges: List[ArrayDDGEdge] = Nil, var outEdges: List[ArrayDDGEdge] = Nil) {
    protected[backend] def fillDot(dot: Digraph): Unit = {
        dot.node(toString, toString, attrs = MMap("shape" -> "oval"))
        outEdges.foreach(e => dot.edge(toString, e.out.toString, e.toString))
    }

    override def toString: String = ind.toString
}

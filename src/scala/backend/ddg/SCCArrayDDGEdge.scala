package scala.backend.ddg

class SCCArrayDDGEdge(val in: SCCArrayDDGNode, val out: SCCArrayDDGNode, val arrayDDGEdge: ArrayDDGEdge) {
    override def toString: String = arrayDDGEdge.toString
}

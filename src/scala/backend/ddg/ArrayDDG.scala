package scala.backend.ddg

import com.liangdp.graphviz4s.Digraph

import scala.backend.vectorization.LoopArrayDeps

class ArrayDDG(val nodes: Array[ArrayDDGNode]) {
    def addDep(stmt1: Int, stmt2: Int, distance: Int): Unit = {
        val lNode = nodes(stmt1)
        val rNode = nodes(stmt2)

        val edge = ArrayDDGEdge(lNode, rNode, distance)

        edge.in.outEdges ::= edge
        edge.out.inEdges ::= edge
    }

    def toPdf(filename: String, view: Boolean = false): Unit = {
        val dot = new Digraph(comment = "Array data dependency graph")

        /* Fill graph */

        nodes.foreach(_.fillDot(dot))

        /* Output tree */

        dot.render(fileName = filename, directory = "resrc/test/res", cleanUp = true, view = view)
    }
}

object ArrayDDG {
    def fromLoopArrayDeps(loopArrayDeps: LoopArrayDeps): ArrayDDG = {
        val ddg = new ArrayDDG(loopArrayDeps.arrayDeps
            .zipWithIndex
            .map(el => new ArrayDDGNode(el._2, el._1))
            .toArray)

        for (i <- loopArrayDeps.arrayDeps.indices) {
            for (j <- loopArrayDeps.arrayDeps.indices) {
                val iElem = loopArrayDeps.arrayDeps(i).elem
                /* TODO: write-write case */
                loopArrayDeps.arrayDeps(j).deps
                    .filter(_.name equals iElem.name)
                    .foreach(jElem => ddg.addDep(i, j, iElem.index.offset - jElem.index.offset))
            }
        }

        ddg
    }
}

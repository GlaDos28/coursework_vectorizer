package scala.backend.vectorization

import scala.backend.ct.ControlTree
import scala.backend.ddg.{ArrayDDG, InductiveVar}
import scala.backend.ct.regions.{Block, While}
import scala.backend.ir.nodes._

object Vectorizer {
    def getLoopsArrayDDGs(ct: ControlTree): Map[While, ArrayDDG] =
        getLoopsWithArrayDeps(ct).map(loopArrayDeps => loopArrayDeps.loop -> ArrayDDG.fromLoopArrayDeps(loopArrayDeps)).toMap

    def getLoopsWithArrayDeps(ct: ControlTree): List[LoopArrayDeps] = {
        val inductVars = ct.getDDG.getInductiveVars
        ct.getRegionsDeep[While].flatMap(loop => getLoopArrayDeps(loop)(inductVars))
    }

    def getLoopArrayDeps(loop: While)(implicit inductVars: Traversable[InductiveVar]): Option[LoopArrayDeps] = {
        if (!loop.bodyRegion.isInstanceOf[Block]) {
            return None
        }

        val stmts = loop.bodyRegion.asInstanceOf[Block].statements

        if (stmts.exists(stmt => !stmt.isInstanceOf[Assign] && !stmt.isInstanceOf[Phi])) {
            return None
        }

        val assigns = stmts
            .filter(_.isInstanceOf[Assign])
            .map(_.asInstanceOf[Assign])
            .filter(assign => !inductVars.exists(_.identName equals assign.lhv.mainIdent.name))

        if (assigns.exists(!_.lhv.isInstanceOf[Access])) {
            return None
        }

        if (assigns.exists(_.lhv.asInstanceOf[Access].exprs.size != 1)) {
            return None
        }

        if (assigns.exists(assign => !CanonicalIndex.canonizable(assign.lhv.asInstanceOf[Access].exprs.head))) {
            return None
        }

        val rawArrayDeps = assigns.map(assign => (
            assign.lhv.asInstanceOf[Access],
            assign.getRHVElemsDeep[Access].filter(access => assigns.exists(_.lhv.asInstanceOf[Access].ident.name equals access.ident.name))
        ))

        if (rawArrayDeps.exists(_._2.exists(access => !CanonicalIndex.canonizable(access.exprs.head)))) {
            return None
        }

        /* --- */

        val e = rawArrayDeps.head._1.exprs.head

        val ident = e match {
            case vi: VersionedIdent => vi
            case _ => e.asInstanceOf[BinExpr].left.asInstanceOf[VersionedIdent]
        }

        val inductIdent = inductVars.find(_.getCondIdent equals ident).get

        loop.initValue = Some(inductIdent.initValue)

        /* --- */

        Some(new LoopArrayDeps(
            loop,
            rawArrayDeps.map(rawArrayDep => ArrayDep(
                ArrayElem.byAccess(rawArrayDep._1),
                rawArrayDep._2.map(ArrayElem.byAccess)
            ))
        ))
    }
}

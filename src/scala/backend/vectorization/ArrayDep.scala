package scala.backend.vectorization

import Preamble._

case class ArrayDep(elem: ArrayElem, deps: List[ArrayElem]) {
    override def toString: String = "%s <- %s" % (elem, deps.mkString(", "))
}

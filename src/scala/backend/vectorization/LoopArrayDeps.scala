package scala.backend.vectorization

import scala.backend.ct.regions.While

class LoopArrayDeps(val loop: While, val arrayDeps: List[ArrayDep]) {
    override def toString: String = arrayDeps.mkString("\n")
}

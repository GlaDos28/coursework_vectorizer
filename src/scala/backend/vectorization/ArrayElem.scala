package scala.backend.vectorization

import Preamble._
import scala.backend.ddg.InductiveVar
import scala.backend.ir.nodes.Access

case class ArrayElem(name: String, index: CanonicalIndex) {
    override def toString: String = "%s[%s]" % (name, index)
}

object ArrayElem {
    def byAccess(access: Access)(implicit inductVars: Traversable[InductiveVar]): ArrayElem =
        ArrayElem(access.ident.name, CanonicalIndex.canonize(access.exprs.head))
}

package scala.backend.vectorization

import Preamble._
import scala.backend.ir.nodes._
import scala.backend.ddg.InductiveVar

case class CanonicalIndex(factor: Int, offset: Int) {
    override def toString: String = {
        val factorStr = factor match {
            case 1  => ""
            case -1 => "-"
            case _  => factor.toString
        }

        offset match {
            case 0               => "%si" % factorStr
            case _ if offset < 0 => "%si - %s" % (factorStr, -offset)
            case _               => "%si + %s" % (factorStr,  offset)
        }
    }
}

object CanonicalIndex {
    def canonizable(e: Expr)(implicit inductVars: Traversable[InductiveVar]): Boolean =
        e.isInstanceOf[VersionedIdent]                                                                     &&
        inductVars.exists(_.getCondIdent equals e.asInstanceOf[VersionedIdent])                            ||
        (e.isInstanceOf[Sum] || e.isInstanceOf[Dif])                                                       &&
        e.asInstanceOf[BinExpr].left.isInstanceOf[VersionedIdent]                                          &&
        inductVars.exists(_.getCondIdent equals e.asInstanceOf[BinExpr].left.asInstanceOf[VersionedIdent]) &&
        e.asInstanceOf[BinExpr].right.isInstanceOf[Const]

    /* Works assuming canonizable(expr) returns true */
    def canonize(e: Expr)(implicit inductVars: Traversable[InductiveVar]): CanonicalIndex = {
        val ident = e match {
            case vi: VersionedIdent => vi
            case _ => e.asInstanceOf[BinExpr].left.asInstanceOf[VersionedIdent]
        }

        val inductIdent = inductVars.find(_.getCondIdent equals ident).get
        val summand = e match {
            case sum: Sum => sum.right.asInstanceOf[Const].value.asInstanceOf[Int]
            case dif: Dif => -dif.right.asInstanceOf[Const].value.asInstanceOf[Int]
            case _        => 0
        }

        CanonicalIndex(inductIdent.step, summand + inductIdent.initValue)
    }
}

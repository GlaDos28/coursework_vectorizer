package scala.backend.ct

object RegionType extends Enumeration {
    type RegionType = Value

    val
    BLOCK,
    CHAIN,
    IF,
    IF_ELSE,
    WHILE,
    WHILE_VECTORIZED
    : RegionType = Value
}

package scala.backend.ct.regions

import scala.backend.ct.RegionType
import scala.backend.ir.nodes.{Expr, Node, Statement, Block => IRBlock, IfElse => IRIfElse}
import scala.collection.mutable.ListBuffer
import scala.reflect.ClassTag

class IfElse(val condRegion: Region, val tRegion: Region, val fRegion: Region) extends Region(RegionType.IF_ELSE) {
    override def regions: List[Region] = List(this) ::: condRegion.regions ::: tRegion.regions ::: fRegion.regions
    override def reifyIRStmts: List[Node] = {
        val condStmts = condRegion.reifyIRStmts
        val preCond   = condStmts.slice(0, condStmts.length - 1)

        preCond ::: List(new IRIfElse(
            condStmts.last.asInstanceOf[Expr],
            new IRBlock(tRegion.reifyIRStmts.map(_.asInstanceOf[Statement]).to[ListBuffer]),
            new IRBlock(fRegion.reifyIRStmts.map(_.asInstanceOf[Statement]).to[ListBuffer])))
    }
    override def getRegionsDeep[T <: Region : ClassTag]: List[T] = (this match {
        case t: T => List(t)
        case _    => Nil
    }) ::: condRegion.getRegionsDeep[T] ::: tRegion.getRegionsDeep[T] ::: fRegion.getRegionsDeep[T]
    override def toString: String = "If-Else"
}

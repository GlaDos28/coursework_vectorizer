package scala.backend.ct.regions

import scala.backend.ct.RegionType
import scala.backend.ir.nodes.{Expr, Node, Statement, Block => IRBlock, If => IRIf}
import scala.collection.mutable.ListBuffer
import scala.reflect.ClassTag

class If(val condRegion: Region, val bodyRegion: Region) extends Region(RegionType.IF) {
    override def regions: List[Region] = List(this) ::: condRegion.regions ::: bodyRegion.regions
    override def reifyIRStmts: List[Node] = {
        val condStmts = condRegion.reifyIRStmts
        val preCond   = condStmts.slice(0, condStmts.length - 1)

        preCond ::: List(new IRIf(
            condStmts.last.asInstanceOf[Expr],
            new IRBlock(bodyRegion.reifyIRStmts.map(_.asInstanceOf[Statement]).to[ListBuffer])))
    }
    override def getRegionsDeep[T <: Region : ClassTag]: List[T] = (this match {
        case t: T => List(t)
        case _    => Nil
    }) ::: condRegion.getRegionsDeep[T] ::: bodyRegion.getRegionsDeep[T]
    override def toString: String = "If"
}

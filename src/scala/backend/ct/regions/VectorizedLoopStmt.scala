package scala.backend.ct.regions

import scala.backend.ct.RegionType
import scala.backend.ir.nodes.{Node, Statement, VectorizedStatement}
import scala.reflect.ClassTag

class VectorizedLoopStmt(val stmt: Statement, from: Int, to: Int) extends Region(RegionType.WHILE_VECTORIZED) {
    override def regions: List[Region] = List(this)
    override def reifyIRStmts: List[Node] = new VectorizedStatement(stmt, from, to) :: Nil
    override def getRegionsDeep[T <: Region : ClassTag]: List[T] = this match {
        case t: T => List(t)
        case _    => Nil
    }
    override def toString: String = stmt.toString + " | from " + from + " to " + to
}

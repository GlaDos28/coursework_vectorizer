package scala.backend.ct.regions

import scala.backend.ct.RegionType
import scala.backend.ir.nodes.Node
import scala.reflect.ClassTag

class Chain(val region1: Region, val region2: Region) extends Region(RegionType.CHAIN) {
    override def regions: List[Region] = List(this) ::: region1.regions ::: region2.regions
    override def getRegionsDeep[T <: Region : ClassTag]: List[T] = (this match {
        case t: T => List(t)
        case _    => Nil
    }) ::: region1.getRegionsDeep[T] ::: region2.getRegionsDeep[T]
    override def reifyIRStmts: List[Node] = region1.reifyIRStmts ++ region2.reifyIRStmts
    override def toString: String = "Chain"
}

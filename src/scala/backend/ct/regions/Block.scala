package scala.backend.ct.regions

import scala.backend.ct.RegionType
import scala.backend.ir.nodes.Node
import scala.reflect.ClassTag

class Block(val statements: List[Node]) extends Region(RegionType.BLOCK) {
    override def regions: List[Region] = List(this)
    override def reifyIRStmts: List[Node] = statements
    override def getRegionsDeep[T <: Region : ClassTag]: List[T] = this match {
        case t: T => List(t)
        case _    => Nil
    }
    override def toString: String = statements
        .map(_.toString)
        .reduceLeftOption(_ + "\n" + _)
        .getOrElse("(empty)")
        .toString
}

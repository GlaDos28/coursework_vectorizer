package scala.backend.ct.regions

import scala.backend.ct.RegionType
import scala.backend.ir.nodes.{Expr, Node, Statement, Block => IRBlock, While => IRWhile}
import scala.collection.mutable.ListBuffer
import scala.reflect.ClassTag

class While(val condRegion: Region, val bodyRegion: Region,
            var initValue: Option[Int] = None, var lastValue: Option[Int] = None) extends Region(RegionType.WHILE) {

    var vectorized: Option[List[Region]] = None

    override def regions: List[Region] = vectorized match {
        case Some(regs) => this :: condRegion.regions ::: bodyRegion.regions ::: regs.flatMap(_.regions)
        case _          => this :: condRegion.regions ::: bodyRegion.regions
    }
    override def reifyIRStmts: List[Node] = {
        vectorized match {
            case Some(regions) => regions.flatMap(_.reifyIRStmts)
            case _ =>
                val condStmts = condRegion.reifyIRStmts
                val condInd   = condStmts.indexWhere(_.isInstanceOf[Expr])

                var (preCond, postCond) = condStmts.splitAt(condInd)
                val expr = postCond.head.asInstanceOf[Expr]

                postCond = postCond.tail

                List(new IRWhile(
                    expr,
                    new IRBlock((preCond ::: postCond ::: bodyRegion.reifyIRStmts).map(_.asInstanceOf[Statement]).to[ListBuffer]))
                )
        }
    }
    override def getRegionsDeep[T <: Region : ClassTag]: List[T] = (this match {
        case t: T => List(t)
        case _    => Nil
    }) ::: condRegion.getRegionsDeep[T] ::: bodyRegion.getRegionsDeep[T]
    override def toString: String = "While" + (if (vectorized.isDefined) " (vectorized)" else "")
}

package scala.backend.ct.regions

import scala.backend.ct.RegionType.RegionType
import scala.backend.ir.nodes.Node
import scala.reflect.ClassTag

abstract class Region protected(val regionType: RegionType, var parent: Option[Region] = None) {
    def regions: List[Region]
    def reifyIRStmts: List[Node]
    def getRegionsDeep[T <: Region : ClassTag]: List[T]

    final def setParentToSubNodesDeep(): Unit = {
        regions.filter(_ != this).foreach { r =>
            r.parent = Some(this)
            r.setParentToSubNodesDeep()
        }
    }

    override def toString: String
}

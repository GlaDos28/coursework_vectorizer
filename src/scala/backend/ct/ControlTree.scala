package scala.backend.ct

import com.liangdp.graphviz4s.Digraph

import scala.util.methods.intTimes
import scala.backend.cfg.CFG
import scala.util.methods.stringEscaped
import scala.backend.ct.regions._
import scala.collection.mutable.{ListBuffer, HashMap => MHashMap, HashSet => MHashSet, Map => MMap, Stack => MStack}
import scala.backend.cfg.{Block => CfgBlock}
import scala.backend.ddg.DDG
import scala.backend.ir.IR
import scala.backend.ir.nodes.{BinExpr, Const, Program, Statement, VarBlock, Block => IRBlock, LT, GT}
import scala.reflect.ClassTag

class ControlTree private(val varBlock: VarBlock, val root: Region) {

    def reifyIR: IR = {
        val stmts = root.reifyIRStmts
        new IR(new Program(varBlock, new IRBlock(stmts
            .slice(1, stmts.length - 1)
            .map(_.asInstanceOf[Statement])
            .to[ListBuffer])))
    }

    def getDDG: DDG = DDG.fromCT(this)

    def getRegionsDeep[T <: Region : ClassTag]: List[T] = root.getRegionsDeep[T]

    def setDDGLoops(ddgLoops: Map[While, List[(List[Int], Boolean)]]): Unit = {
        ddgLoops.foreach { ddgLoop =>
            val loop = ddgLoop._1
            val vectorizedLoops = ddgLoop._2.map {
                case (stmtInd :: Nil, vectorizable) if vectorizable =>
                    val stmt = loop.bodyRegion.asInstanceOf[Block].statements(stmtInd)
                    new VectorizedLoopStmt(stmt.asInstanceOf[Statement], loop.initValue.get, loop.lastValue.get)
                case (stmtInds, _) => new While(
                    loop.condRegion,
                    new Block(stmtInds.map(loop.bodyRegion.asInstanceOf[Block].statements(_)))
                )
            }

            loop.vectorized = Some(vectorizedLoops)
        }
    }

    def toPdf(filename: String, view: Boolean = false): Unit = {
        val dot = new Digraph(comment = "Control tree")
        val regions = root.regions

        /* Set region IDs and add nodes */

        var idCounter = 0
        val idMap = MHashMap[Region, Int]()

        regions.foreach(region => {
            idMap += (region -> idCounter)
            idCounter += 1

            dot.node(idMap(region).toString, region match {
                case r: Block  => r.statements
                    .map(_.toString.escaped)
                    .reduceLeftOption(_ + "\n" + _)
                    .getOrElse("(empty)")
                    .toString
                case some => some.toString
            }, attrs = MMap("shape" -> "rectangle"))
        })

        /* Put edges */

        regions.foreach({
            case r: Chain  =>
                dot.edge(idMap(r).toString, idMap(r.region1).toString)
                dot.edge(idMap(r).toString, idMap(r.region2).toString)
            case r: If     =>
                dot.edge(idMap(r).toString, idMap(r.condRegion).toString)
                dot.edge(idMap(r).toString, idMap(r.bodyRegion).toString)
            case r: IfElse =>
                dot.edge(idMap(r).toString, idMap(r.condRegion).toString)
                dot.edge(idMap(r).toString, idMap(r.tRegion).toString)
                dot.edge(idMap(r).toString, idMap(r.fRegion).toString)
            case r: While if r.vectorized.isDefined  =>
                dot.edge(idMap(r).toString, idMap(r.condRegion).toString)

                r.vectorized.get.foreach{subR =>
                    dot.edge(idMap(r).toString, idMap(subR).toString)
                }
            case r: While  =>
                dot.edge(idMap(r).toString, idMap(r.condRegion).toString)
                dot.edge(idMap(r).toString, idMap(r.bodyRegion).toString)
            case _ =>
        })

        /* Output tree */

        dot.render(fileName = filename, directory = "resrc/test/res", cleanUp = true, view = view)
    }

}

object ControlTree {

    def fromCFG(cfg: CFG): ControlTree = {
        val cfgCopy     = cfg.copy
        val regionStack = MStack[(Region, CfgBlock)]()

        cfgCopy.buildDomTree()

        cfgCopy.inputBlock.statements  += new Const("START")
        cfgCopy.outputBlock.statements += new Const("END")

        traverseBlock(cfgCopy.inputBlock, regionStack)

        regionStack.top._1.setParentToSubNodesDeep()

        new ControlTree(cfg.varBlock, regionStack.top._1)
    }

    private def traverseBlock(
        block:   CfgBlock,
        regions: MStack[(Region, CfgBlock)],
        visited: MHashSet[CfgBlock] = MHashSet()): Unit = {

        if (visited.contains(block)) {
            return
        }

        visited += block
        regions.push((new Block(block.statements.map(_.copy).toList), block))

        val next1 = block.n1
        val next2 = block.n2

        reduceTillPossible(regions)

        if (next1.isDefined && next1.get.ps.forall(prev => visited.contains(prev) || (next1.get dom prev))) {
            traverseBlock(next1.get, regions, visited)
        }

        if (next2.isDefined && next2.get.ps.forall(prev => visited.contains(prev) || (next2.get dom prev))) {
            traverseBlock(next2.get, regions, visited)
        }
    }

    private def reduceTillPossible(regions: MStack[(Region, CfgBlock)]): Unit = {
        var stop = false

        while (!stop) {
            regions.toList match {
                case reg2 :: reg1 :: _ if isChain(reg1._2, reg2._2) =>
                    2 times regions.pop

                    reg1._2.n1 = reg2._2.n1
                    reg1._2.n2 = reg2._2.n2

                    if (reg2._2.n1.isDefined) { reg2._2.n1.get.changePrev(reg2._2, reg1._2) }
                    if (reg2._2.n2.isDefined) { reg2._2.n2.get.changePrev(reg2._2, reg1._2) }

                    regions.push((new Chain(reg1._1, reg2._1), reg1._2))
                case body :: cond :: _ if isIf(cond._2, body._2) =>
                    2 times regions.pop

                    val next = cond._2.n2.get

                    cond._2.n1 = Some(next)
                    cond._2.n2 = None

                    next.removePrev(body._2)

                    regions.push((new If(cond._1, body._1), cond._2))
                case cond :: body :: _ if isIf(cond._2, body._2) =>
                    2 times regions.pop

                    val next = cond._2.n2.get

                    cond._2.n1 = Some(next)
                    cond._2.n2 = None

                    next.removePrev(body._2)

                    regions.push((new If(cond._1, body._1), cond._2))
                case fBody :: next :: tBody :: cond :: _ if isIfElse(cond._2, tBody._2, fBody._2) =>
                    4 times regions.pop

                    cond._2.n1 = Some(next._2)
                    cond._2.n2 = None

                    next._2.removePrev(tBody._2)
                    next._2.removePrev(fBody._2)
                    next._2.ps += cond._2

                    regions.push((new IfElse(cond._1, tBody._1, fBody._1), cond._2))
                    regions.push(next)
                case fBody :: tBody :: cond :: _ if isIfElse(cond._2, tBody._2, fBody._2) =>
                    3 times regions.pop

                    val next = tBody._2.n1.get

                    cond._2.n1 = Some(next)
                    cond._2.n2 = None

                    next.ps -= tBody._2
                    next.ps -= fBody._2
                    next.ps += cond._2

                    regions.push((new IfElse(cond._1, tBody._1, fBody._1), cond._2))
                case body :: cond :: _ if isWhile(cond._2, body._2) =>
                    2 times regions.pop

                    cond._2.n1 = Some(cond._2.n2.get)
                    cond._2.n2 = None
                    cond._2.ps -= body._2

                    var lastValue: Option[Int] = None

                    if (cond._1.isInstanceOf[Block] &&
                        cond._1.asInstanceOf[Block].statements.last.isInstanceOf[BinExpr] &&
                        cond._1.asInstanceOf[Block].statements.last.asInstanceOf[BinExpr].right.isInstanceOf[Const])
                    {
                        lastValue = Some(cond._1.asInstanceOf[Block].statements.last.asInstanceOf[BinExpr].right.asInstanceOf[Const].value.asInstanceOf[Int])

                        if (cond._1.asInstanceOf[Block].statements.last.isInstanceOf[LT] || cond._1.asInstanceOf[Block].statements.last.isInstanceOf[GT]) {
                            lastValue = Some(lastValue.get - 1)
                        }
                    }

                    regions.push((new While(cond._1, body._1, None, lastValue), cond._2))
                case cond :: body :: _ if isWhile(cond._2, body._2) =>
                    2 times regions.pop

                    cond._2.n1 = Some(cond._2.n2.get)
                    cond._2.n2 = None
                    cond._2.ps -= body._2

                    var lastValue: Option[Int] = None

                    if (cond._1.isInstanceOf[Block] &&
                        cond._1.asInstanceOf[Block].statements.last.isInstanceOf[BinExpr] &&
                        cond._1.asInstanceOf[Block].statements.last.asInstanceOf[BinExpr].right.isInstanceOf[Const])
                    {
                        lastValue = Some(cond._1.asInstanceOf[Block].statements.last.asInstanceOf[BinExpr].right.asInstanceOf[Const].value.asInstanceOf[Int])

                        if (cond._1.asInstanceOf[Block].statements.last.isInstanceOf[LT] || cond._1.asInstanceOf[Block].statements.last.isInstanceOf[GT]) {
                            lastValue = Some(lastValue.get - 1)
                        }
                    }

                    regions.push((new While(cond._1, body._1, None, lastValue), cond._2))
                case _ => stop = true
            }
        }
    }

    private def isChain(block1: CfgBlock, block2: CfgBlock): Boolean =
        block1.n1.isDefined && block1.n1.get == block2 && block1.n2.isEmpty &&
            block2.ps.length == 1 && block2.ps.head == block1 &&
            block2.nextIter().length < 2

    private def isIf(cond: CfgBlock, body: CfgBlock): Boolean =
        cond.n1.isDefined && cond.n1.get == body &&
            cond.n2.isDefined && body.n1.isDefined &&
            cond.n2.get == body.n1.get &&
            body.n2.isEmpty

    private def isIfElse(cond: CfgBlock, tBody: CfgBlock, fBody: CfgBlock): Boolean =
        cond.n1.isDefined && cond.n1.get == tBody &&
            cond.n2.isDefined && cond.n2.get == fBody &&
            tBody.n1.isDefined && fBody.n1.isDefined &&
            tBody.n1.get == fBody.n1.get &&
            tBody.n2.isEmpty &&
            fBody.n2.isEmpty

    private def isWhile(cond: CfgBlock, body: CfgBlock): Boolean =
        cond.n1.isDefined && cond.n1.get == body &&
            cond.n2.isDefined && cond.n2.get != body &&
            body.n1.isDefined && body.n1.get == cond &&
            body.n2.isEmpty

}

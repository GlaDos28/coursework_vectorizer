package scala.backend.ir


import com.liangdp.graphviz4s.Digraph

import scala.backend.codeText.CodeText
import scala.backend.ir.nodes._
import scala.util.{Copyable, Counter}


class IR(var root: Program = new Program()) extends Copyable[IR] {

    def reifyCode: CodeText = new CodeText(root.codeDump.toString)

    def toPdf(filename: String, view: Boolean = false): Unit = {
        val dot = new Digraph(comment = "Intermediate representation")

        /* Fill graph */

        root.fillDot(Counter(), None, dot)

        /* Output tree */

        dot.render(fileName = filename, directory = "resrc/test/res", cleanUp = true, view = view)
    }

    def setRoot(rootBlock: Block): Unit = {
        this.root.block = rootBlock
    }

    def appendVarInfo(varName: String, varType: String, initValue: Any): Unit =
        this.root.varBlock.appendVarDef(new VarDef(varName, varType, initValue))

    def withoutSSA: IR = new IR(root.copyWithoutSSA)

    override def toString: String = reifyCode.text
    override def copy: IR = new IR(root.copy)

}


object IR {

    def fromCode(code: CodeText): IR = code.ir

}

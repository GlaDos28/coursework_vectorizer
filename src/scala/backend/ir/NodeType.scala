package scala.backend.ir

object NodeType extends Enumeration {
    type NodeType = Value

    val
    Program,
    VarBlock,
    VarDef,
    Block,
    Constant,
    Identifier,
    Access,
    Assign,
    FunctionCall,
    Sum,
    Dif,
    Mul,
    Div,
    Mod,
    LT,
    GT,
    ELT,
    EGT,
    Eq,
    NEq,
    And,
    Or,
    Xor,
    Not,
    UnaryMinus,
    If,
    IfElse,
    While,
    Comment,
    Phi,
    VectorizedStatement
    : NodeType = Value
}

package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.reflect.ClassTag
import scala.util.GetSet

class Const(val value: Any) extends Node(NodeType.Constant) with Expr {
    override def idents: List[Ident] = Nil
    override def rhvIdents: List[GetSet[Ident]] = Nil
    override def getRHVElemsDeep[T : ClassTag]: List[T] = this match {
        case t: T => List(t)
        case _    => Nil
    }

    override def subNodes: List[Node] = Nil
    override def selfString: String = toString
    override def codeDump: CodeDump = value match {
        case true  => "True"
        case false => "False"
        case _     => value.toString
    }
    override def copy: Const = new Const(value)
    override def copyWithoutSSA: Const = new Const(value)
}

object Const {
    implicit def intToConst(intValue: Int): Const = new Const(intValue)
}

package scala.backend.ir.nodes

trait Statement extends Node {
    override def copy: Statement
    override def copyWithoutSSA: Statement
}

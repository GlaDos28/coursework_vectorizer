package scala.backend.ir.nodes

import scala.util.ExtendedInt._
import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.collection.mutable.ArrayBuffer
import scala.util.GetSet

class Phi(var ident: Ident, val indices: ArrayBuffer[Int]) extends Node(NodeType.Phi)
    with Statement
    with HasLHV {

    def withIdent:  Boolean = true
    def withAccess: Boolean = false

    override def lhvIdent: GetSet[Ident] = GetSet.by[Ident](
        () => ident.asInstanceOf[Ident],
        (newIdent) => ident = newIdent)
    override def idents: List[Ident] = List(ident)

    override def subNodes: List[Node] = ident :: indices.map(new VersionedIdent(ident.name, _)).toList
    override def selfString: String = "ϕ"
    override def codeDump: CodeDump = "%s = ϕ(%s)".format(ident.codeDump, indices
        .map(ident.name + _.toSubscript)
        .reduce(_ + ", " + _))
    override def copy: Phi = new Phi(ident.copy, indices.clone())
    override def copyWithoutSSA: Phi = throw new RuntimeException("phi can not be copied into IR without SSA")
}

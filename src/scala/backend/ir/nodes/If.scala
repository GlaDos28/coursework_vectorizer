package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.reflect.ClassTag
import scala.util.GetSet

class If(val condition: Expr, val body: Block) extends Node(NodeType.If) with IfLike {

    override def idents: List[Ident] = condition.idents
    override def rhvIdents: List[GetSet[Ident]] = condition.rhvIdents
    override def getRHVElemsDeep[T : ClassTag]: List[T] = condition.getRHVElemsDeep[T]

    override def subNodes: List[Node] = List(condition, body)
    override def selfString: String = "if"
    override def codeDump: CodeDump = new CodeDump(List(
        "if %s:".format(condition.codeDump),
        body.conjoinedCodeDump
    ), false)
    override def copy: If = new If(condition.copy, body.copy)
    override def copyWithoutSSA: If = new If(condition.copyWithoutSSA, body.copyWithoutSSA)
}

package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class LT(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.LT, leftExpr, rightExpr, "<") {
    override def copy: LT = new LT(this.left.copy, this.right.copy)
    override def copyWithoutSSA: LT = new LT(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

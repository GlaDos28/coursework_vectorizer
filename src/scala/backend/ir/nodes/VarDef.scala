package scala.backend.ir.nodes

import Preamble._
import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType

class VarDef(val name: String, val valueType: String, val initValue: Any) extends Node(NodeType.VarDef) {

    override def subNodes: List[Node] = Nil
    override def selfString: String = toString
    override def codeDump: CodeDump = toString
    override def copy: VarDef = new VarDef(name, valueType, initValue)  /* Keep in mind, initValue is not copied => causes with mutable types */
    override def copyWithoutSSA: VarDef = copy
    override def toString: String = "%s: %s = %s" % (name, valueType, initValue.toString)

}

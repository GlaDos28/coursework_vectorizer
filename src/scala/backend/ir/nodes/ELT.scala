package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class ELT(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.ELT, leftExpr, rightExpr, "<=") {
    override def copy: ELT = new ELT(this.left.copy, this.right.copy)
    override def copyWithoutSSA: ELT = new ELT(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

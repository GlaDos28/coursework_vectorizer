package scala.backend.ir.nodes

trait IfLike extends Statement with HasRHV {
    override def copy: IfLike
    override def copyWithoutSSA: IfLike
}

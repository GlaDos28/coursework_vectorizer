package scala.backend.ir.nodes

import Preamble._
import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.reflect.ClassTag
import scala.util.GetSet

class Assign(var lhv: AccessLike, var rhv: Expr) extends Node(NodeType.Assign)
    with Statement
    with HasRHV
    with HasLHV {

    def withIdent:  Boolean = lhv.nodeType == NodeType.Identifier
    def withAccess: Boolean = lhv.nodeType == NodeType.Access

    override def lhvIdent: GetSet[Ident] = GetSet.by[Ident](
        () => lhv.mainIdent,
        (ident) => lhv match {
            case _:      Ident  => lhv = ident
            case access: Access => access.setIdent(ident)
        })
    override def idents: List[Ident] = lhv.idents ::: rhv.idents
    override def rhvIdents: List[GetSet[Ident]] =
        if (rhv.nodeType == NodeType.Identifier) List(GetSet.by[Ident](
            () => rhv.asInstanceOf[Ident],
            (ident) => rhv = ident))
        else (if (lhv.nodeType == NodeType.Access)
            lhv.asInstanceOf[Access].rhvIdentsNoMain else
            lhv.rhvIdents) ::: rhv.rhvIdents

    override def getRHVElemsDeep[T : ClassTag]: List[T] = rhv.getRHVElemsDeep[T]

    override def selfString: String = "="
    override def codeDump: CodeDump = toString
    override def subNodes: List[Node] = List(lhv, rhv)
    override def copy: Assign = new Assign(lhv.copy, rhv.copy)
    override def copyWithoutSSA: Assign = new Assign(lhv.copyWithoutSSA, rhv.copyWithoutSSA)
    override def toString: String = "%s = %s" % (lhv.codeDump, rhv.codeDump)

}

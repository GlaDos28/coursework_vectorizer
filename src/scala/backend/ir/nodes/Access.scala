package scala.backend.ir.nodes


import Preamble._
import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.reflect.ClassTag
import scala.util.GetSet


class Access(var ident: Ident = new Ident(""), var exprs: List[Expr] = Nil) extends Node(NodeType.Access) with AccessLike {
    def setIdent(ident: Ident): Access = {
        this.ident = ident
        this
    }


    def prependExpr(expr: Expr): Access = {
        this.exprs ::= expr
        this
    }


    def rhvIdentsNoMain: List[GetSet[Ident]] = {
        var res = List[GetSet[Ident]]()

        for (i <- exprs.indices) {
            val expr = exprs(i)

            val exprRhvIdents =
                if (expr.nodeType == NodeType.Identifier) List(GetSet.by[Ident](
                    () => expr.asInstanceOf[Ident],
                    (ident) => exprs = exprs.updated(i, ident)))
                else expr.rhvIdents

            res :::= exprRhvIdents
        }

        res
    }

    override def mainIdent: Ident = ident
    override def idents: List[Ident] = ident :: exprs.map(_.idents).reduceLeft(_ ::: _)
    override def rhvIdents: List[GetSet[Ident]] = GetSet.by[Ident](
        () => ident,
        (newIdent) => ident = newIdent
    ) :: rhvIdentsNoMain
    override def getRHVElemsDeep[T : ClassTag]: List[T] = (this match {
        case t: T => List(t)
        case _    => Nil
    }) ::: exprs.map(_.getRHVElemsDeep[T]).reduceLeft(_ ::: _)

    override def subNodes: List[Node] = exprs
    override def selfString: String = "%s[...]" % ident.name
    override def codeDump: CodeDump = toString
    override def copy: Access = new Access(ident.copy, exprs.map(_.copy))
    override def copyWithoutSSA: Access = new Access(ident.copyWithoutSSA, exprs.map(_.copyWithoutSSA))
    override def toString: String = ident.toString + exprs.map("[" + _ + "]").mkString
}



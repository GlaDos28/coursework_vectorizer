package scala.backend.ir.nodes

import com.liangdp.graphviz4s.Digraph

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType.NodeType
import scala.collection.mutable
import scala.util.{Copyable, Counter}

abstract class Node protected(val nodeType: NodeType) extends Copyable[Node] {
    def codeDump: CodeDump
    def subNodes: List[Node]
    def selfString: String
    def copyWithoutSSA: Node

    protected[backend] def fillDot(counter: Counter, parentId: Option[Int], dot: Digraph): Unit = {
        val id = counter.next

        dot.node(id.toString, selfString, attrs = mutable.Map("shape" -> "oval"))

        if (parentId.isDefined) {
            dot.edge(parentId.get.toString, id.toString)
        }

        subNodes.foreach(_.fillDot(counter, Some(id), dot))
    }

    override def copy: Node
    override def toString: String = codeDump.toString
}

package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.reflect.ClassTag
import scala.util.GetSet

class IfElse(val condition: Expr, val tBody: Block, val fBody: Block) extends Node(NodeType.IfElse) with IfLike {

    override def idents: List[Ident] = condition.idents
    override def rhvIdents: List[GetSet[Ident]] = condition.rhvIdents
    override def getRHVElemsDeep[T : ClassTag]: List[T] = condition.getRHVElemsDeep[T]

    override def subNodes: List[Node] = List(condition, tBody, fBody)
    override def selfString: String = "if-else"
    override def codeDump: CodeDump = new CodeDump(List(
        "if %s:".format(condition.codeDump),
        tBody.conjoinedCodeDump,
        "else:",
        fBody.conjoinedCodeDump
    ), false)
    override def copy: IfElse = new IfElse(condition.copy, tBody.copy, fBody.copy)
    override def copyWithoutSSA: IfElse = new IfElse(condition.copyWithoutSSA, tBody.copyWithoutSSA, fBody.copyWithoutSSA)
}

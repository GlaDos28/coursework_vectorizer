package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class Mul(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.Mul, leftExpr, rightExpr, "*") {
    override def copy: Mul = new Mul(this.left.copy, this.right.copy)
    override def copyWithoutSSA: Mul = new Mul(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

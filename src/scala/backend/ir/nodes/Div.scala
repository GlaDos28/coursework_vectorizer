package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class Div(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.Div, leftExpr, rightExpr, "/") {
    override def copy: Div = new Div(this.left.copy, this.right.copy)
    override def copyWithoutSSA: Div = new Div(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

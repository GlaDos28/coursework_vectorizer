package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class Dif(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.Dif, leftExpr, rightExpr, "-") {
    override def copy: Dif = new Dif(this.left.copy, this.right.copy)
    override def copyWithoutSSA: Dif = new Dif(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

package scala.backend.ir.nodes

import scala.util.GetSet

trait HasLHV extends HasValue {
    def lhvIdent: GetSet[Ident]

    def withIdent:  Boolean
    def withAccess: Boolean

    override def copy: HasLHV
    override def copyWithoutSSA: HasLHV
}

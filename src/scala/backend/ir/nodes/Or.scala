package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class Or(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.Or, leftExpr, rightExpr, "or") {
    override def copy: Or = new Or(this.left.copy, this.right.copy)
    override def copyWithoutSSA: Or = new Or(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

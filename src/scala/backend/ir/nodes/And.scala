package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class And(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.And, leftExpr, rightExpr, "and") {
    override def copyWithoutSSA: And = new And(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
    override def copy: And = new And(this.left.copy, this.right.copy)

}

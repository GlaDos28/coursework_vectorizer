package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class UnaryMinus(exprExpr: Expr) extends UnExpr(NodeType.UnaryMinus, exprExpr, "-") {
    override def copy: UnaryMinus = new UnaryMinus(this.expr.copy)
    override def copyWithoutSSA: UnaryMinus = new UnaryMinus(this.expr.copyWithoutSSA)
}

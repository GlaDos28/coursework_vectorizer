package scala.backend.ir.nodes

trait Expr extends Node with HasRHV {
    override def copy: Expr
    override def copyWithoutSSA: Expr
}

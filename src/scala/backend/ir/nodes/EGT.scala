package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class EGT(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.EGT, leftExpr, rightExpr, ">=") {
    override def copy: EGT = new EGT(this.left.copy, this.right.copy)
    override def copyWithoutSSA: EGT = new EGT(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump.stringToCodeDump
import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.collection.mutable.ListBuffer
import scala.util.methods.seqInsertBetweenIf

class Block(val statements: ListBuffer[Statement]) extends Node(NodeType.Block) {

    def this(statements: Statement*) = this(statements.to[ListBuffer])


    def conjoinedCodeDump: CodeDump = stmtsCodeDump


    def prependStmt(stmt: Statement): Block = {
        this.statements.prepend(stmt)
        this
    }


    def appendStmt(stmt: Statement): Block = {
        this.statements.append(stmt)
        this
    }


    override def selfString: String = "{ ... }"
    override def subNodes: List[Node] = statements.toList
    override def codeDump: CodeDump = stmtsCodeDump
    override def copy: Block = new Block(statements.map(_.copy))
    override def copyWithoutSSA: Block = new Block(statements.filter(_.nodeType != NodeType.Phi).map(_.copyWithoutSSA))


    private def stmtsCodeDump: CodeDump = new CodeDump(statements
        .map({
            case s @ (_: Assign | _: Phi) => stringToCodeDump(s.codeDump.toString)
            case s         => s.codeDump
        })
        .insertBetweenIf((stmt1, stmt2) => (stmt1, stmt2) match {
            case (s1, s2) =>
                (s1.str.isDefined && s2.str.isEmpty) ||
                (s1.str.isEmpty   && s2.str.isDefined)
        }, "\n")
        .toList)

}

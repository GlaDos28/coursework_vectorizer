package scala.backend.ir.nodes

trait HasValue extends Statement {
    def idents: List[Ident]

    override def copy: HasValue
    override def copyWithoutSSA: HasValue
}

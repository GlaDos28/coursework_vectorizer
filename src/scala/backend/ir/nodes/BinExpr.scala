package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.backend.ir.NodeType.NodeType
import scala.reflect.ClassTag
import scala.util.GetSet

class BinExpr(nodeType: NodeType, var left: Expr, var right: Expr, val opStr: String) extends Node(nodeType) with Expr {
    override def idents: List[Ident] = left.idents ::: right.idents
    override def rhvIdents: List[GetSet[Ident]] =
        (if (left.nodeType  == NodeType.Identifier) List(GetSet.by[Ident](
            () => left.asInstanceOf[Ident],
            (ident) => left  = ident))
        else left.rhvIdents) :::
        (if (right.nodeType == NodeType.Identifier) List(GetSet.by[Ident](
            () => right.asInstanceOf[Ident],
            (ident) => right = ident))
        else right.rhvIdents)
    override def getRHVElemsDeep[T : ClassTag]: List[T] = (this match {
        case t: T => List(t)
        case _    => Nil
    }) ::: left.getRHVElemsDeep[T] ::: right.getRHVElemsDeep[T]

    override def subNodes: List[Node] = List(left, right)
    override def selfString: String = opStr
    override def codeDump: CodeDump = "(%s %s %s)".format(left.codeDump, opStr, right.codeDump)
    override def copy: BinExpr = new BinExpr(this.nodeType, left.copy, right.copy, opStr)
    override def copyWithoutSSA: BinExpr = new BinExpr(this.nodeType, left.copyWithoutSSA, right.copyWithoutSSA, opStr)
}

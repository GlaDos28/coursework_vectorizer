package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType

class Comment(val text: String) extends Node(NodeType.Comment) with Statement {
    override def subNodes: List[Node] = Nil
    override def selfString: String = toString
    override def codeDump: CodeDump = "/* %s */".format(text)
    override def copy: Comment = new Comment(text)
    override def copyWithoutSSA: Comment = new Comment(text)
}

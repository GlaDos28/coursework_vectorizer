package scala.backend.ir.nodes

trait AccessLike extends Expr {

    def mainIdent: Ident

    override def copyWithoutSSA: AccessLike
    override def copy: AccessLike
}

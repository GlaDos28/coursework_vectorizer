package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class Sum(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.Sum, leftExpr, rightExpr, "+") {
    override def copy: Sum = new Sum(this.left.copy, this.right.copy)
    override def copyWithoutSSA: Sum = new Sum(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

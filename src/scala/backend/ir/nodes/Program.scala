package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType

class Program private[backend](val varBlock: VarBlock = new VarBlock(), var block: Block = new Block()) extends Node(NodeType.Program) {
    override def subNodes: List[Node] = List(block)
    override def selfString: String = "Program"
    override def codeDump: CodeDump = new CodeDump(varBlock.codeDump :: block.codeDump :: Nil)
    override def copy: Program = new Program(varBlock.copy, block.copy)
    override def copyWithoutSSA: Program = new Program(varBlock.copyWithoutSSA, block.copyWithoutSSA)
}

package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class Xor(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.Xor, leftExpr, rightExpr, "xor") {
    override def copy: Xor = new Xor(this.left.copy, this.right.copy)
    override def copyWithoutSSA: Xor = new Xor(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

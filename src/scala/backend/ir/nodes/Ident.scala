package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.reflect.ClassTag
import scala.util.GetSet

class Ident(var name: String) extends Node(NodeType.Identifier) with AccessLike {
    override def mainIdent: Ident = this
    override def idents: List[Ident] = List(this)
    override def rhvIdents: List[GetSet[Ident]] = Nil
    override def getRHVElemsDeep[T : ClassTag]: List[T] = this match {
        case t: T => List(t)
        case _    => Nil
    }

    override def subNodes: List[Node] = Nil
    override def selfString: String = toString
    override def codeDump: CodeDump = name
    override def copy: Ident = new Ident(name)
    override def copyWithoutSSA: Ident = new Ident(name)
    override def toString: String = name
    override def hashCode(): Int = toString.hashCode
    override def equals(obj: scala.Any): Boolean = obj.isInstanceOf[Ident] &&
        obj.asInstanceOf[Ident].name == name
}

object Ident {
    implicit def toIdent(varName: String): Ident = new Ident(varName)
}

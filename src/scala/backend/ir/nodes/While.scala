package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.reflect.ClassTag
import scala.util.GetSet

class While(val condition: Expr, val body: Block) extends Node(NodeType.While)
    with Statement
    with HasRHV {

    override def idents: List[Ident] = condition.idents
    override def rhvIdents: List[GetSet[Ident]] = condition.rhvIdents
    override def getRHVElemsDeep[T : ClassTag]: List[T] = condition.getRHVElemsDeep[T]

    override def subNodes: List[Node] = List(condition, body)
    override def selfString: String = "while"
    override def codeDump: CodeDump = new CodeDump(List(
        "while %s:".format(condition.codeDump),
        body.conjoinedCodeDump
    ), false)
    override def copy: While = new While(condition.copy, body.copy)
    override def copyWithoutSSA: While = new While(condition.copyWithoutSSA, body.copyWithoutSSA)
}

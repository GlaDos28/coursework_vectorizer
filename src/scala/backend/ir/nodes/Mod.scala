package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class Mod(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.Mod, leftExpr, rightExpr, "%") {
    override def copy: Mod = new Mod(this.left.copy, this.right.copy)
    override def copyWithoutSSA: Mod = new Mod(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

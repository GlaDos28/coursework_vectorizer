package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class NEq(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.NEq, leftExpr, rightExpr, "!=") {
    override def copy: NEq = new NEq(this.left.copy, this.right.copy)
    override def copyWithoutSSA: NEq = new NEq(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

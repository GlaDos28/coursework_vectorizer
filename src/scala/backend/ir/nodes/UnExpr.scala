package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType
import scala.backend.ir.NodeType.NodeType
import scala.reflect.ClassTag
import scala.util.GetSet

class UnExpr(nodeType: NodeType, var expr: Expr, val opStr: String) extends Node(nodeType) with Expr {
    override def idents: List[Ident] = expr.idents
    override def rhvIdents: List[GetSet[Ident]] =
        if (expr.nodeType == NodeType.Identifier) List(GetSet.by[Ident](
            () => expr.asInstanceOf[Ident],
            (ident) => expr = ident))
        else expr.rhvIdents
    override def getRHVElemsDeep[T : ClassTag]: List[T] = (this match {
        case t: T => List(t)
        case _    => Nil
    }) ::: expr.getRHVElemsDeep[T]
    override def subNodes: List[Node] = List(expr)
    override def selfString: String = opStr
    override def codeDump: CodeDump = "(%s%s)".format(opStr, expr.codeDump)
    override def copy: UnExpr = new UnExpr(this.nodeType, expr.copy, opStr)
    override def copyWithoutSSA: UnExpr = new UnExpr(this.nodeType, expr.copyWithoutSSA, opStr)
}

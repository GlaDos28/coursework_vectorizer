package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.codeText.CodeDump.stringToCodeDump
import scala.backend.ir.NodeType
import scala.util.methods.seqInsertBetweenIf

class VarBlock(var varDefs: List[VarDef] = Nil) extends Node(NodeType.VarBlock) {


    def prependVarDef(varDef: VarDef): Unit =
        varDefs +:= varDef


    def appendVarDef(varDef: VarDef): Unit =
        varDefs :+= varDef


    override def subNodes: List[Node] = varDefs
    override def selfString: String = "VarBlock"
    override def codeDump: CodeDump =
        if (varDefs.isEmpty)
            "" else
            new CodeDump("var:" :: varDefsCodeDump :: Nil)
    override def copy: VarBlock = new VarBlock(varDefs.map(_.copy))
    override def copyWithoutSSA: VarBlock = new VarBlock(varDefs.map(_.copy))


    private def varDefsCodeDump: CodeDump = new CodeDump(varDefs
        .map(_.codeDump)
        .insertBetweenIf((stmt1, stmt2) => (stmt1, stmt2) match {
            case (s1, s2) =>
                (s1.str.isDefined && s2.str.isEmpty) ||
                    (s1.str.isEmpty   && s2.str.isDefined)
        }, "\n")
        .toList)

}

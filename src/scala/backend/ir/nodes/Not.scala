package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class Not(exprExpr: Expr) extends UnExpr(NodeType.Not, exprExpr, "not") {
    override def copy: Not = new Not(this.expr.copy)
    override def copyWithoutSSA: Not = new Not(this.expr.copyWithoutSSA)
}

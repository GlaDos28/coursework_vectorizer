package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class Eq(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.Eq, leftExpr, rightExpr, "==") {
    override def copy: Eq = new Eq(this.left.copy, this.right.copy)
    override def copyWithoutSSA: Eq = new Eq(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

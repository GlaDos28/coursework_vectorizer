package scala.backend.ir.nodes

import scala.backend.ir.NodeType

class GT(leftExpr: Expr, rightExpr: Expr) extends BinExpr(NodeType.GT, leftExpr, rightExpr, ">") {
    override def copy: GT = new GT(this.left.copy, this.right.copy)
    override def copyWithoutSSA: GT = new GT(this.left.copyWithoutSSA, this.right.copyWithoutSSA)
}

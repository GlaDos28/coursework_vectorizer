package scala.backend.ir.nodes

import scala.reflect.ClassTag
import scala.util.GetSet

trait HasRHV extends HasValue {
    def rhvIdents: List[GetSet[Ident]]
    def getRHVElemsDeep[T : ClassTag]: List[T]

    override def copy: HasRHV
    override def copyWithoutSSA: HasRHV
}

package scala.backend.ir.nodes

import scala.util.ExtendedInt._
import scala.backend.codeText.CodeDump

class VersionedIdent(name: String, val version: Int) extends Ident(name) {
    override def idents: List[VersionedIdent] = List(this)
    override def selfString: String = toString
    override def codeDump: CodeDump = toString
    override def copy: VersionedIdent = new VersionedIdent(name, version)
    override def copyWithoutSSA: Ident = new Ident(name)
    override def toString: String = name + version.toSubscript
    override def equals(obj: scala.Any): Boolean = obj.isInstanceOf[VersionedIdent] &&
        obj.asInstanceOf[VersionedIdent].name == name &&
        obj.asInstanceOf[VersionedIdent].version == version
}

package scala.backend.ir.nodes

import scala.backend.codeText.CodeDump
import scala.backend.ir.NodeType

class VectorizedStatement(val baseStmt: Statement, val from: Int, val to: Int) extends Node(NodeType.VectorizedStatement) with Statement {
    override def codeDump:       CodeDump            = toString
    override def subNodes:       List[Node]          = Nil
    override def selfString:     String              = "= (vectorized)"
    override def copy:           VectorizedStatement = new VectorizedStatement(baseStmt.copy, from, to)
    override def copyWithoutSSA: VectorizedStatement = new VectorizedStatement(baseStmt.copyWithoutSSA, from, to)
    override def toString:       String              = baseStmt.toString + " { from " + from + " to " + to + " }"
}

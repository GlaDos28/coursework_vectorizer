package scala

import scala.backend.cfg.CFG
import scala.backend.codeText.CodeText
import scala.backend.ct.ControlTree
import scala.backend.ct.regions.While
import scala.backend.ddg.SCCArrayDDG
import scala.backend.ir.IR
import scala.backend.vectorization.Vectorizer

object cli {
    val MsgUsage         = "Usage: arg1 - code file path"
    val CodeFileTemplate = "resrc/test/res/%s.txt"

    val FilenameCode        = "Test-code"
    val FileNameCodeSSA     = "Test-code-SSA"
    val FilenameIR          = "Test-IR"
    val FilenameIRSSA       = "Test-IR-SSA"
    val FilenameCFG         = "Test-CFG"
    val FilenameCFGSSA      = "Test-CFG-SSA"
    val FilenameCT          = "Test-CT"
    val FilenameDom         = "Test-dom-tree"
    val FilenamePDom        = "Test-pdom-tree"
    val FilenameDDG         = "Test-DDG"
    val FilenameDDGSSA      = "Test-DDG-SSA"
    val FilenameArrayDDG    = "Test-array-DDG"
    val FilenameSCCArrayDDG = "Test-SCC-array-DDG"

    def main(args: Array[String]): Unit = {
        /* Hardcode console input arguments */

        val finalArgs = Array("resrc/test/text.txt") // val finalArgs = args

        /* Process console input arguments */

        if (finalArgs.length != 1) {
            println(MsgUsage)
            return
        }

        val filePath = finalArgs(0)

        /* Process data */

        val code         = CodeText.fromFile(filePath)
        val ir           = IR.fromCode(code)
        val cfg          = CFG.fromIR(ir).simplified
        val ddg          = cfg.controlTree.getDDG
        val cfgSsa       = cfg.withSSA
        val ddgSsa       = cfgSsa.controlTree.getDDG
        val irSsa        = cfgSsa.reifyIR
        val codeSsa      = irSsa.reifyCode
        val controlTree  = cfgSsa.controlTree
        val domTree      = cfgSsa.domTree.get
        val arraysDdg    = Vectorizer.getLoopsArrayDDGs(controlTree)
        val sccArraysDDG = arraysDdg.mapValues(arrayDdg => SCCArrayDDG.fromArrayDDG(arrayDdg))

        val ct = ControlTree.fromCFG(cfg)

        /* Output results */

        IR.fromCode(code).reifyCode.toFile(CodeFileTemplate.format(FilenameCode))
        codeSsa                    .toFile(CodeFileTemplate.format(FileNameCodeSSA))
        ir                         .toPdf(FilenameIR)
        irSsa                      .toPdf(FilenameIRSSA)
        cfg                        .toPdf(FilenameCFG)
        cfgSsa                     .toPdf(FilenameCFGSSA)
        controlTree                .toPdf(FilenameCT)
        domTree                    .domToPdf(FilenameDom)
        domTree                    .pdomToPdf(FilenamePDom)
        ddg                        .toPdf(FilenameDDG)
        ddgSsa                     .toPdf(FilenameDDGSSA)
        arraysDdg.head._2          .toPdf(FilenameArrayDDG)
        sccArraysDDG.head._2       .toPdf(FilenameSCCArrayDDG)

        controlTree.setDDGLoops(sccArraysDDG.mapValues(_.getLoopsWithStatementIds))
        val irVectorized   = controlTree.reifyIR.withoutSSA
        val codeVectorized = irVectorized.reifyCode

        controlTree   .toPdf("TEMP-CT-Vectorized")
        irVectorized  .toPdf("TEMP-IR-Vectorized")
        codeVectorized.toFile(CodeFileTemplate.format("TEMP-Code-Vectorized"))

        println("Inductive variables: " + ddgSsa.getInductiveVars)
        println("Loops with array deps: " + Vectorizer.getLoopsWithArrayDeps(controlTree))
        println("Test: " + sccArraysDDG.head._2.getLoopsWithStatementIds)
    }
}

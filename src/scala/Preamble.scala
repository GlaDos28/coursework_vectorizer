package scala


object Preamble {

    /* ----====---- Implicits ----====---- */

    /* String */

    implicit class StringExtraOps(s: String) {
        def %(param: Any):        String = s.format(param)
        def %(params: List[Any]): String = s.format(params: _*)
        def %(params: Product):   String = %(params.productIterator.toList)
    }

}

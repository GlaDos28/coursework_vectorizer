package javalang;

import _lib_language_generator_.structures.bnf.BNF;
import _lib_language_generator_.structures.bnf.productions.BNF_BasicProduction;
import _lib_language_generator_.structures.bnf.productions.BNF_LexemeProduction;
import _lib_language_generator_.structures.bnf.productions.misc.elements.BNF_NT;
import _lib_language_generator_.structures.bnf.productions.misc.elements.BNF_SRule;
import _lib_language_generator_.structures.bnf.productions.misc.elements.BNF_String;
import _lib_language_generator_.structures.bnf.productions.misc.elements.Element;
import javalang.javaIR.*;

import java.util.Stack;
import java.util.ArrayList;

public final class LangCompiler {
	public static final BNF BNF = new BNF();

	static {
		//** BNF initialization program fragment (semantic rule No.0)

		Stack<JavaIRNode> nodeStack = new Stack<>();

		//** nonterminals declaration

		int _Program_ = BNF.addNonterminal(new BNF_BasicProduction("Program"));
		int _Block_ = BNF.addNonterminal(new BNF_BasicProduction("Block"));
		int _Statements_ = BNF.addNonterminal(new BNF_BasicProduction("Statements"));
		int _Statement_ = BNF.addNonterminal(new BNF_BasicProduction("Statement"));
		int _FuncCall_ = BNF.addNonterminal(new BNF_BasicProduction("FuncCall"));
		int _Params_ = BNF.addNonterminal(new BNF_BasicProduction("Params"));
		int _OptParams_ = BNF.addNonterminal(new BNF_BasicProduction("OptParams"));
		int _Assign_ = BNF.addNonterminal(new BNF_BasicProduction("Assign"));
		int _Condition_ = BNF.addNonterminal(new BNF_BasicProduction("Condition"));
		int _ElsePart_ = BNF.addNonterminal(new BNF_BasicProduction("ElsePart"));
		int _Cycle_ = BNF.addNonterminal(new BNF_BasicProduction("Cycle"));
		int _Expression_ = BNF.addNonterminal(new BNF_BasicProduction("Expression"));
		int _OptExpression_ = BNF.addNonterminal(new BNF_BasicProduction("OptExpression"));
		int _Factor_ = BNF.addNonterminal(new BNF_BasicProduction("Factor"));

		int _If_ = BNF.addLexeme(new BNF_LexemeProduction("If"));
		int _Else_ = BNF.addLexeme(new BNF_LexemeProduction("Else"));
		int _While_ = BNF.addLexeme(new BNF_LexemeProduction("While"));
		int _Boolean_ = BNF.addLexeme(new BNF_LexemeProduction("Boolean"));
		int _Ident_ = BNF.addLexeme(new BNF_LexemeProduction("Ident"));
		int _String_ = BNF.addLexeme(new BNF_LexemeProduction("String"));
		int _Number_ = BNF.addLexeme(new BNF_LexemeProduction("Number"));
		int _OrOp_ = BNF.addLexeme(new BNF_LexemeProduction("OrOp"));
		int _AndOp_ = BNF.addLexeme(new BNF_LexemeProduction("AndOp"));
		int _XorOp_ = BNF.addLexeme(new BNF_LexemeProduction("XorOp"));
		int _CompOp_ = BNF.addLexeme(new BNF_LexemeProduction("CompOp"));
		int _AddOp_ = BNF.addLexeme(new BNF_LexemeProduction("AddOp"));
		int _MulOp_ = BNF.addLexeme(new BNF_LexemeProduction("MulOp"));
		int _NotOp_ = BNF.addLexeme(new BNF_LexemeProduction("NotOp"));
		int _AssignOp_ = BNF.addLexeme(new BNF_LexemeProduction("AssignOp"));
		int _OpenParent_ = BNF.addLexeme(new BNF_LexemeProduction("OpenParent"));
		int _CloseParent_ = BNF.addLexeme(new BNF_LexemeProduction("CloseParent"));
		int _OpenBrace_ = BNF.addLexeme(new BNF_LexemeProduction("OpenBrace"));
		int _CloseBrace_ = BNF.addLexeme(new BNF_LexemeProduction("CloseBrace"));
		int _Comma_ = BNF.addLexeme(new BNF_LexemeProduction("Comma"));
		int _Semicolon_ = BNF.addLexeme(new BNF_LexemeProduction("Semicolon"));
		int _OptIdent_ = BNF.addLexeme(new BNF_LexemeProduction("OptIdent"));
		int _OptString_ = BNF.addLexeme(new BNF_LexemeProduction("OptString"));
		int _OptNumber_ = BNF.addLexeme(new BNF_LexemeProduction("OptNumber"));
		int _Symbol_ = BNF.addLexeme(new BNF_LexemeProduction("Symbol"));
		int _LowerLetter_ = BNF.addLexeme(new BNF_LexemeProduction("LowerLetter"));
		int _UpperLetter_ = BNF.addLexeme(new BNF_LexemeProduction("UpperLetter"));
		int _Letter_ = BNF.addLexeme(new BNF_LexemeProduction("Letter"));
		int _Digit_ = BNF.addLexeme(new BNF_LexemeProduction("Digit"));

		//** nonterminals products implementation

		BNF.getNonterminal(_Program_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Block_),
				new BNF_SRule(tokens -> () -> CompileResult.result = (Block) nodeStack.pop())
			}
		});

		BNF.getNonterminal(_Block_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_OpenBrace_),
				new BNF_SRule(tokens -> () -> nodeStack.push(null)),
				new BNF_NT(_Statements_),
				new BNF_SRule(tokens -> () -> {
    ArrayList<JavaIRNode> stmts = new ArrayList<>();
    JavaIRNode cur = nodeStack.pop();
    while (cur != null) {
        stmts.add(cur);
        cur = nodeStack.pop();
    }
    nodeStack.push(new Block(stmts));
}),
				new BNF_NT(_CloseBrace_)
			}
		});

		BNF.getNonterminal(_Statements_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Statement_),
				new BNF_NT(_Statements_)
			},
			new Element[] {

			}
		});

		BNF.getNonterminal(_Statement_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_FuncCall_),
				new BNF_NT(_Semicolon_)
			},
			new Element[] {
				new BNF_NT(_Assign_),
				new BNF_NT(_Semicolon_)
			},
			new Element[] {
				new BNF_NT(_Condition_)
			},
			new Element[] {
				new BNF_NT(_Cycle_)
			}
		});

		BNF.getNonterminal(_FuncCall_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Ident_),
				new BNF_SRule(tokens -> () -> {
    nodeStack.push(new Ident(tokens[0].getValue()));
    nodeStack.push(null); /* marker of function parameters' ending */
}),
				new BNF_NT(_OpenParent_),
				new BNF_NT(_Params_),
				new BNF_NT(_CloseParent_),
				new BNF_SRule(tokens -> () -> {
    ArrayList<JavaIRNode> params = new ArrayList<>();
    JavaIRNode cur = nodeStack.pop();
    while (cur != null) {
        params.add(cur);
        cur = nodeStack.pop();
    }
    nodeStack.push(new FuncCall(nodeStack.pop(), params));
})
			}
		});

		BNF.getNonterminal(_Params_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Expression_),
				new BNF_NT(_OptParams_)
			},
			new Element[] {

			}
		});

		BNF.getNonterminal(_OptParams_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Comma_),
				new BNF_NT(_Expression_),
				new BNF_NT(_OptParams_)
			},
			new Element[] {

			}
		});

		BNF.getNonterminal(_Assign_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Ident_),
				new BNF_SRule(tokens -> () -> nodeStack.push(new Ident(tokens[0].getValue()))),
				new BNF_NT(_AssignOp_),
				new BNF_NT(_Expression_),
				new BNF_SRule(tokens -> () -> nodeStack.push(new Assign(nodeStack.pop(), nodeStack.pop())))
			}
		});

		BNF.getNonterminal(_Condition_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_If_),
				new BNF_NT(_OpenParent_),
				new BNF_NT(_Expression_),
				new BNF_NT(_CloseParent_),
				new BNF_NT(_Block_),
				new BNF_NT(_ElsePart_)
			}
		});

		BNF.getNonterminal(_ElsePart_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Else_),
				new BNF_NT(_Block_),
				new BNF_SRule(tokens -> () -> nodeStack.push(new IfElse(nodeStack.pop(), nodeStack.pop(), nodeStack.pop())))
			},
			new Element[] {
				new BNF_SRule(tokens -> () -> nodeStack.push(new If(nodeStack.pop(), nodeStack.pop())))
			}
		});

		BNF.getNonterminal(_Cycle_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_While_),
				new BNF_NT(_OpenParent_),
				new BNF_NT(_Expression_),
				new BNF_NT(_CloseParent_),
				new BNF_NT(_Block_),
				new BNF_SRule(tokens -> () -> nodeStack.push(new While(nodeStack.pop(), nodeStack.pop())))
			}
		});

		BNF.getNonterminal(_Expression_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Factor_),
				new BNF_NT(_OptExpression_)
			}
		});

		BNF.getNonterminal(_OptExpression_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_AddOp_),
				new BNF_NT(_Factor_),
				new BNF_SRule(tokens -> () -> nodeStack.push(tokens[0].getValue().equals("+") ? new Sum(nodeStack.pop(), nodeStack.pop()) : new Sub(nodeStack.pop(), nodeStack.pop()))),
				new BNF_NT(_OptExpression_)
			},
			new Element[] {

			}
		});

		BNF.getNonterminal(_Factor_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_OpenParent_),
				new BNF_NT(_Expression_),
				new BNF_NT(_CloseParent_)
			},
			new Element[] {
				new BNF_NT(_FuncCall_)
			},
			new Element[] {
				new BNF_NT(_Ident_),
				new BNF_SRule(tokens -> () -> nodeStack.push(new Ident(tokens[0].getValue())))
			},
			new Element[] {
				new BNF_NT(_String_),
				new BNF_SRule(tokens -> () -> nodeStack.push(new Const(tokens[0].getValue())))
			},
			new Element[] {
				new BNF_NT(_Number_),
				new BNF_SRule(tokens -> () -> nodeStack.push(new Const(tokens[0].getValue())))
			},
			new Element[] {
				new BNF_NT(_Boolean_),
				new BNF_SRule(tokens -> () -> nodeStack.push(new Const(tokens[0].getValue())))
			}
		});

		BNF.getLexeme(_If_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("if")
			}
		});

		BNF.getLexeme(_Else_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("else")
			}
		});

		BNF.getLexeme(_While_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("while")
			}
		});

		BNF.getLexeme(_Boolean_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("true")
			},
			new Element[] {
				new BNF_String("false")
			}
		});

		BNF.getLexeme(_Ident_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Letter_),
				new BNF_NT(_OptIdent_)
			},
			new Element[] {
				new BNF_String("_"),
				new BNF_NT(_OptIdent_)
			}
		});

		BNF.getLexeme(_String_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("\""),
				new BNF_NT(_OptString_),
				new BNF_String("\"")
			}
		});

		BNF.getLexeme(_Number_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Digit_),
				new BNF_NT(_OptNumber_)
			}
		});

		BNF.getLexeme(_OrOp_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("||")
			}
		});

		BNF.getLexeme(_AndOp_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("&&")
			}
		});

		BNF.getLexeme(_XorOp_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("^^")
			}
		});

		BNF.getLexeme(_CompOp_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("==")
			},
			new Element[] {
				new BNF_String("!=")
			},
			new Element[] {
				new BNF_String(">=")
			},
			new Element[] {
				new BNF_String("<=")
			},
			new Element[] {
				new BNF_String(">")
			},
			new Element[] {
				new BNF_String("<")
			}
		});

		BNF.getLexeme(_AddOp_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("+")
			},
			new Element[] {
				new BNF_String("-")
			}
		});

		BNF.getLexeme(_MulOp_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("*")
			},
			new Element[] {
				new BNF_String("/")
			},
			new Element[] {
				new BNF_String("%")
			}
		});

		BNF.getLexeme(_NotOp_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("!")
			}
		});

		BNF.getLexeme(_AssignOp_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("=")
			}
		});

		BNF.getLexeme(_OpenParent_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("(")
			}
		});

		BNF.getLexeme(_CloseParent_).setElements(new Element[][] {
			new Element[] {
				new BNF_String(")")
			}
		});

		BNF.getLexeme(_OpenBrace_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("{")
			}
		});

		BNF.getLexeme(_CloseBrace_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("}")
			}
		});

		BNF.getLexeme(_Comma_).setElements(new Element[][] {
			new Element[] {
				new BNF_String(",")
			}
		});

		BNF.getLexeme(_Semicolon_).setElements(new Element[][] {
			new Element[] {
				new BNF_String(";")
			}
		});

		BNF.getLexeme(_OptIdent_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Letter_),
				new BNF_NT(_OptIdent_)
			},
			new Element[] {
				new BNF_NT(_Digit_),
				new BNF_NT(_OptIdent_)
			},
			new Element[] {
				new BNF_String("_"),
				new BNF_NT(_OptIdent_)
			},
			new Element[] {

			}
		});

		BNF.getLexeme(_OptString_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Symbol_),
				new BNF_NT(_OptString_)
			},
			new Element[] {

			}
		});

		BNF.getLexeme(_OptNumber_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Digit_),
				new BNF_NT(_OptNumber_)
			},
			new Element[] {

			}
		});

		BNF.getLexeme(_Symbol_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_Digit_)
			},
			new Element[] {
				new BNF_NT(_Letter_)
			},
			new Element[] {
				new BNF_String(" ")
			},
			new Element[] {
				new BNF_String("\\n")
			},
			new Element[] {
				new BNF_String("\\t")
			},
			new Element[] {
				new BNF_String("_")
			},
			new Element[] {
				new BNF_String("\'")
			},
			new Element[] {
				new BNF_String("\\\"")
			},
			new Element[] {
				new BNF_String("!")
			},
			new Element[] {
				new BNF_String("?")
			},
			new Element[] {
				new BNF_String("-")
			},
			new Element[] {
				new BNF_String("+")
			},
			new Element[] {
				new BNF_String("=")
			},
			new Element[] {
				new BNF_String("/")
			},
			new Element[] {
				new BNF_String(",")
			},
			new Element[] {
				new BNF_String("{")
			},
			new Element[] {
				new BNF_String("}")
			},
			new Element[] {
				new BNF_String("[")
			},
			new Element[] {
				new BNF_String("]")
			},
			new Element[] {
				new BNF_String("(")
			},
			new Element[] {
				new BNF_String(")")
			},
			new Element[] {
				new BNF_String("*")
			},
			new Element[] {
				new BNF_String("&")
			},
			new Element[] {
				new BNF_String("^")
			},
			new Element[] {
				new BNF_String("%")
			},
			new Element[] {
				new BNF_String("$")
			},
			new Element[] {
				new BNF_String("#")
			},
			new Element[] {
				new BNF_String("@")
			},
			new Element[] {
				new BNF_String(":")
			},
			new Element[] {
				new BNF_String(";")
			},
			new Element[] {
				new BNF_String(">")
			},
			new Element[] {
				new BNF_String("`")
			},
			new Element[] {
				new BNF_String("~")
			},
			new Element[] {
				new BNF_String(".")
			},
			new Element[] {
				new BNF_String("\\\\")
			},
			new Element[] {
				new BNF_String("<")
			},
			new Element[] {
				new BNF_String("|")
			}
		});

		BNF.getLexeme(_LowerLetter_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("a")
			},
			new Element[] {
				new BNF_String("b")
			},
			new Element[] {
				new BNF_String("c")
			},
			new Element[] {
				new BNF_String("d")
			},
			new Element[] {
				new BNF_String("e")
			},
			new Element[] {
				new BNF_String("f")
			},
			new Element[] {
				new BNF_String("g")
			},
			new Element[] {
				new BNF_String("h")
			},
			new Element[] {
				new BNF_String("i")
			},
			new Element[] {
				new BNF_String("j")
			},
			new Element[] {
				new BNF_String("k")
			},
			new Element[] {
				new BNF_String("l")
			},
			new Element[] {
				new BNF_String("m")
			},
			new Element[] {
				new BNF_String("n")
			},
			new Element[] {
				new BNF_String("o")
			},
			new Element[] {
				new BNF_String("p")
			},
			new Element[] {
				new BNF_String("q")
			},
			new Element[] {
				new BNF_String("r")
			},
			new Element[] {
				new BNF_String("s")
			},
			new Element[] {
				new BNF_String("t")
			},
			new Element[] {
				new BNF_String("u")
			},
			new Element[] {
				new BNF_String("v")
			},
			new Element[] {
				new BNF_String("w")
			},
			new Element[] {
				new BNF_String("x")
			},
			new Element[] {
				new BNF_String("y")
			},
			new Element[] {
				new BNF_String("z")
			}
		});

		BNF.getLexeme(_UpperLetter_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("A")
			},
			new Element[] {
				new BNF_String("B")
			},
			new Element[] {
				new BNF_String("C")
			},
			new Element[] {
				new BNF_String("D")
			},
			new Element[] {
				new BNF_String("E")
			},
			new Element[] {
				new BNF_String("F")
			},
			new Element[] {
				new BNF_String("G")
			},
			new Element[] {
				new BNF_String("H")
			},
			new Element[] {
				new BNF_String("I")
			},
			new Element[] {
				new BNF_String("J")
			},
			new Element[] {
				new BNF_String("K")
			},
			new Element[] {
				new BNF_String("L")
			},
			new Element[] {
				new BNF_String("M")
			},
			new Element[] {
				new BNF_String("N")
			},
			new Element[] {
				new BNF_String("O")
			},
			new Element[] {
				new BNF_String("P")
			},
			new Element[] {
				new BNF_String("Q")
			},
			new Element[] {
				new BNF_String("R")
			},
			new Element[] {
				new BNF_String("S")
			},
			new Element[] {
				new BNF_String("T")
			},
			new Element[] {
				new BNF_String("U")
			},
			new Element[] {
				new BNF_String("V")
			},
			new Element[] {
				new BNF_String("W")
			},
			new Element[] {
				new BNF_String("X")
			},
			new Element[] {
				new BNF_String("Y")
			},
			new Element[] {
				new BNF_String("Z")
			}
		});

		BNF.getLexeme(_Letter_).setElements(new Element[][] {
			new Element[] {
				new BNF_NT(_LowerLetter_)
			},
			new Element[] {
				new BNF_NT(_UpperLetter_)
			}
		});

		BNF.getLexeme(_Digit_).setElements(new Element[][] {
			new Element[] {
				new BNF_String("0")
			},
			new Element[] {
				new BNF_String("1")
			},
			new Element[] {
				new BNF_String("2")
			},
			new Element[] {
				new BNF_String("3")
			},
			new Element[] {
				new BNF_String("4")
			},
			new Element[] {
				new BNF_String("5")
			},
			new Element[] {
				new BNF_String("6")
			},
			new Element[] {
				new BNF_String("7")
			},
			new Element[] {
				new BNF_String("8")
			},
			new Element[] {
				new BNF_String("9")
			}
		});

	}

	private LangCompiler() {}
}

package javalang.javaIR;

import java.util.ArrayList;

public class FuncCall extends JavaIRNode {
	public JavaIRNode name;
	public ArrayList<JavaIRNode> params;

	public FuncCall(JavaIRNode name, ArrayList<JavaIRNode> params) {
		this.name = name;
		this.params = params;
	}
}

package javalang.javaIR;

public class While extends JavaIRNode {
	public JavaIRNode condition, body;

	public While(JavaIRNode body, JavaIRNode condition) {
		this.condition = condition;
		this.body = body;
	}
}

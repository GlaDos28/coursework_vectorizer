package javalang.javaIR;

public class Sum extends JavaIRNode {
	public JavaIRNode left, right;

	public Sum(JavaIRNode right, JavaIRNode left) {
		this.left  = left;
		this.right = right;
	}
}

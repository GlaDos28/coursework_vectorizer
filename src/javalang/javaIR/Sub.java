package javalang.javaIR;

public class Sub extends JavaIRNode {
	public JavaIRNode left, right;

	public Sub(JavaIRNode right, JavaIRNode left) {
		this.left  = left;
		this.right = right;
	}
}

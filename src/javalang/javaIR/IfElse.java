package javalang.javaIR;

public class IfElse extends JavaIRNode {
	public JavaIRNode condition, tBody, fBody;

	public IfElse(JavaIRNode fBody, JavaIRNode tBody, JavaIRNode condition) {
		this.condition = condition;
		this.tBody = tBody;
		this.fBody = fBody;
	}
}

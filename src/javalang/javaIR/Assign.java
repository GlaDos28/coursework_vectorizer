package javalang.javaIR;

public class Assign extends JavaIRNode {
	public JavaIRNode lhv, rhv;

	public Assign(JavaIRNode rhv, JavaIRNode lhv) {
		this.lhv = lhv;
		this.rhv = rhv;
	}
}

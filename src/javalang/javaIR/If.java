package javalang.javaIR;

public class If extends JavaIRNode {
	public JavaIRNode condition, body;

	public If(JavaIRNode body, JavaIRNode condition) {
		this.condition = condition;
		this.body = body;
	}
}

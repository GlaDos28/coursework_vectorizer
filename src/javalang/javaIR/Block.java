package javalang.javaIR;

import java.util.ArrayList;

public class Block extends JavaIRNode {
	public ArrayList<JavaIRNode> stmts;

	public Block(ArrayList<JavaIRNode> stmts) {
		this.stmts = stmts;
	}
}
